#!/bin/bash
php bin/console d:d:d --force #dump schema
php bin/console d:d:c #create db
php bin/console d:s:u --force #pdate update
php bin/console doctrine:fixtures:load -n #rebuild base data
