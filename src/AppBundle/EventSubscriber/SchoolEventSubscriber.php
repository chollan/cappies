<?php
/**
 * Created by PhpStorm.
 * User: cholland
 * Date: 9/10/18
 * Time: 1:42 AM
 */

namespace AppBundle\EventSubscriber;


use AppBundle\Entity\Critic;
use AppBundle\Entity\CriticTeam;
use AppBundle\Entity\SeasonalSchool;
use AppBundle\Event\SchoolEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class SchoolEventSubscriber implements EventSubscriberInterface
{
    private $em;
    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            SchoolEvent::STATUS_CHANGED => [
                ['changeUserStatuses', 10],
                ['generateSchoolCriticTeam', 9]
            ]
        ];
    }

    public function changeUserStatuses(SchoolEvent $schoolEvent){
        if($schoolEvent->getSeasonalSchool()->getStatus() == SeasonalSchool::STATUS_APPROVED){
            $critics = $schoolEvent->getSeasonalSchool()->getCritics();
            foreach($critics as $critic){
                $critic->setActive(true);
                $this->em->persist($critic);
            }
            $this->em->flush();
        }
    }

    public function generateSchoolCriticTeam(SchoolEvent $schoolEvent){
        $seasonalSchool = $schoolEvent->getSeasonalSchool();
        if($seasonalSchool->getStatus() == SeasonalSchool::STATUS_APPROVED){
            $team = new CriticTeam();
            $team->setSchool($seasonalSchool);
            $team->setName($seasonalSchool->getSchool()->getName().' Team');
            $team->setRegional(false);
            foreach($seasonalSchool->getParticipants() as $participant){
                if($participant instanceof Critic){
                    $team->addMember($participant);
                }
            }
            $this->em->persist($team);
            $this->em->flush();
        }
    }
}