<?php

namespace AppBundle\EventSubscriber;

use AppBundle\Entity\Chapter;
use AppBundle\Entity\MappedSuperclass\SeasonalUser;
use AppBundle\Entity\SchoolStaff;
use AppBundle\Entity\SeasonalSchool;
use AppBundle\Event\ChapterEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ChapterEventSubscriber implements EventSubscriberInterface
{
    private $em;
    private $mailer;
    private $templating;
    private $container;

    function __construct(EntityManagerInterface $em, \Swift_Mailer $mailer, \Twig_Environment $templating, ContainerInterface $container){
        $this->em = $em;
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->container = $container;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2')))
     *
     * @return array The event names to listen to
     */
    public static function getSubscribedEvents()
    {
        return [
            ChapterEvent::STATUS_CHANGED => [
                ['createSchoolRegistrationLink', 10],
                ['sendEmailsToAdviser', 9]
            ]
        ];
    }

    public function createSchoolRegistrationLink(ChapterEvent $chapterEvent){
        $chapter = $chapterEvent->getChapter();
        if($chapter->getStatus() == Chapter::STATUS_APPROVED){
            // let's get the schools
            $schools = $chapter->getParticipatingSchools();

            // for each of the schools, set the application link
            // and allow the school to apply for the season
            foreach($schools as $seasonalSchool){
                $seasonalSchool->setApplicationLink(uniqid());
                if($seasonalSchool->getStatus() == SeasonalSchool::STATUS_PENDING_CHAPTER){
                    $seasonalSchool->setStatus(SeasonalSchool::STATUS_APPROVED_TO_APPLY);
                }
                $this->em->persist($seasonalSchool);
            }
            $this->em->flush();
        }
    }

    public function sendEmailsToAdviser(ChapterEvent $chapterEvent){
        $chapter = $chapterEvent->getChapter();
        if($chapter->getStatus() == Chapter::STATUS_APPROVED){
            // let's get the schools
            $schools = $chapter->getParticipatingSchools();

            // for each of the schools, send the apply email to the adviser
            foreach($schools as $seasonalSchool){
                if($seasonalSchool->getStatus() == SeasonalSchool::STATUS_APPROVED_TO_APPLY){
                    // pull the participants
                    foreach($seasonalSchool->getParticipants() as $participant){
                        if($participant instanceof SchoolStaff){
                            if($participant->getGroup()->getName() == 'Adviser'){
                                $this->sendApprovalEmail($chapter, $participant, $seasonalSchool, $participant->getUser()->getEmail());
                            }
                        }
                    }
                }
            }
        }
    }

    private function sendApprovalEmail(Chapter $chapter, SeasonalUser $user, SeasonalSchool $seasonalSchool, $aryTo = []){
        $fromEmail = $this->container->getParameter('from_email_address');
        $fromName = $this->container->getParameter('from_email_sender_name');
        $message = (new \Swift_Message('Cappies Chapter Approved'))
            ->setFrom($fromEmail, $fromName)
            ->setTo($aryTo)
            ->setBody(
                $this->templating->render(
                    'AppBundle:email/txt:chapter_approved.txt.twig',
                    [
                        'chapter' => $chapter,
                        'user' => $user,
                        'seasonalSchool' => $seasonalSchool
                    ]
                ),
                'text/plain'
            )
            /*->addPart(
                $this->renderView(
                // templates/emails/registration.html.twig
                    'emails/registration.html.twig',
                    array('name' => $name)
                ),
                'text/html'
            )*/
        ;

        $this->mailer->send($message);
    }
}