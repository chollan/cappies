<?php
namespace AppBundle\EventSubscriber;

use AppBundle\Event\ReviewEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ReviewEventSubscriber implements EventSubscriberInterface
{


    public static function getSubscribedEvents()
    {
        return [
            ReviewEvent::STATUS_CHANGED => [
                ['statusChanged', 10]
            ]
        ];
    }

    protected function statusChanged(){

    }
}