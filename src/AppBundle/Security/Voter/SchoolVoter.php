<?php
namespace AppBundle\Security\Voter;

use AppBundle\Entity\SeasonalSchool;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class SchoolVoter extends Voter
{
    const CREATE = 'SCHOOL_CREATE';
    const READ = 'SCHOOL_READ';
    const UPDATE = 'SCHOOL_UPDATE';
    const DELETE = 'SCHOOL_DELETE';
    const CHANGE_STATUS = 'SCHOOL_CHANGE_STATUS';

    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports($attribute, $subject)
    {
        $reflectionClass = new \ReflectionClass(__CLASS__);
        return (in_array($attribute, $reflectionClass->getConstants()) && $subject instanceof SeasonalSchool);
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        return true;
        /*$user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        $post = $subject;

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($post, $user);
            case self::EDIT:
                return $this->canEdit($post, $user);
        }

        throw new \LogicException('This code should not be reached!');*/
    }
}