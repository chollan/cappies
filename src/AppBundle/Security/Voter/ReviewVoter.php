<?php
namespace AppBundle\Security\Voter;

use AppBundle\Entity\Review;
use AppBundle\Entity\User;
use AppBundle\Service\SeasonService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AccessDecisionManagerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ReviewVoter extends Voter
{
    const CREATE_REVISION = 'review.createrevision';

    private $decisionManager;
    private $seasonService;

    public function __construct(AccessDecisionManagerInterface $decisionManager, SeasonService $seasonService)
    {
        $this->decisionManager = $decisionManager;
        $this->seasonService = $seasonService;
    }

    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports($attribute, $subject)
    {
        $reflectionClass = new \ReflectionClass(__CLASS__);
        return (in_array($attribute, $reflectionClass->getConstants()) && $subject instanceof Review);
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $review, TokenInterface $token)
    {
        //return true;
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        if ($this->decisionManager->decide($token, [User::ROLE_SUPER_ADMIN])) {
            return true;
        }

        switch ($attribute) {
            case self::CREATE_REVISION:
                return $this->canCreateRevision($review, $token);
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canCreateRevision(Review $review, TokenInterface $token){
        if ($this->decisionManager->decide($token, array(User::ROLE_CRITIC))) {
            if($review->getStatus() == Review::STATUS_IN_PROGRESS || $review->getStatus() == Review::STATUS_REJECTED || $review->getStatus() == Review::STATUS_NOT_STARTED){
                return true;
            }
        }
        return false;
    }
}