<?php
namespace AppBundle\Security\Voter;

use AppBundle\Entity\SchoolShow;
use AppBundle\Entity\User;
use AppBundle\Service\SeasonService;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class ShowVoter extends Voter
{
    private $seasonService;

    const VOLUNTEER = 'show.volunteer';
    const UNVOLUNTEER = 'show.unvolunteer';

    function __construct(SeasonService $seasonService)
    {
        $this->seasonService = $seasonService;
    }

    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports($attribute, $subject)
    {
        $reflectionClass = new \ReflectionClass(__CLASS__);
        return (in_array($attribute, $reflectionClass->getConstants()) && $subject instanceof SchoolShow);
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $schoolShow, TokenInterface $token)
    {
        return true;
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        switch ($attribute) {
            case self::UNVOLUNTEER:
                return $this->canUnVolunteer($schoolShow, $user);
            /*case self::EDIT:
                return $this->canEdit($post, $user);*/
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canUnVolunteer(SchoolShow $show, User $user){
        dump($show);
        dump($user);
        exit;
    }
}