<?php
namespace AppBundle\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class SeasonalSchoolVoter extends Voter
{
    const CREATE = 'SEASONAL_SCHOOL_CREATE';
    const READ = 'SEASONAL_SCHOOL_READ';
    const UPDATE = 'SEASONAL_SCHOOL_UPDATE';
    const DELETE = 'SEASONAL_SCHOOL_DELETE';
    const CHANGE_STATUS = 'SEASONAL_SCHOOL_CHANGE_STATUS';


    /**
     * Determines if the attribute and subject are supported by this voter.
     *
     * @param string $attribute An attribute
     * @param mixed $subject The subject to secure, e.g. an object the user wants to access or any other PHP type
     *
     * @return bool True if the attribute and subject are supported, false otherwise
     */
    protected function supports($attribute, $subject)
    {
        $reflectionClass = new \ReflectionClass(__CLASS__);
        return (in_array($attribute, $reflectionClass->getConstants()) && $subject instanceof SeasonalSchool);
    }

    /**
     * Perform a single access check operation on a given attribute, subject and token.
     * It is safe to assume that $attribute and $subject already passed the "supports()" method check.
     *
     * @param string $attribute
     * @param mixed $subject
     * @param TokenInterface $token
     *
     * @return bool
     */
    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        return true;
    }
}