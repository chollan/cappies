<?php

namespace AppBundle\Service;

use AppBundle\Entity\BoardMember;
use AppBundle\Entity\Chapter;
use AppBundle\Entity\Critic;
use AppBundle\Entity\Season;
use AppBundle\Entity\User;
use AppBundle\Entity\SchoolStaff;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;

class SeasonService
{
    private $em;
    private $tokenStorage;

    function __construct(EntityManagerInterface $em, TokenStorageInterface $tokenStorage)
    {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    public function getCurrentSeason(){
        $token = $this->tokenStorage->getToken();
        if($token instanceof PostAuthenticationGuardToken){
            $user = $token->getUser();
            if($user instanceof User){
                return $user->getSeasonLoggedIn();
            }
        }
        return $this->em->getRepository(Season::class)->getCurrentSeason();
    }

    public function getChapterFromUser(User $user){
        $chapterRepo = $this->em->getRepository(Chapter::class);

        $chapter = null;
        $activeSeason = $user->getActiveSeason();
        if($activeSeason instanceof Critic){
            $chapter = $chapterRepo->findOneByCritic($activeSeason);
        }elseif($activeSeason instanceof BoardMember){
            $chapter = $chapterRepo->findOneByBoardMember($activeSeason);
        }elseif($activeSeason instanceof SchoolStaff){
            $chapter = $chapterRepo->findOneBySchoolStaff($activeSeason);
        }
        return $chapter;
    }
}