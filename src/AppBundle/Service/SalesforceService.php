<?php
/**
 * Created by PhpStorm.
 * User: cholland
 * Date: 9/11/18
 * Time: 12:09 AM
 */

namespace AppBundle\Service;


use AppBundle\Entity\BoardMember;
use AppBundle\Entity\Chapter;
use AppBundle\Entity\Critic;
use AppBundle\Entity\Group;
use AppBundle\Entity\SchoolStaff;
use AppBundle\Entity\SeasonalSchool;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class SalesforceService
{
    private $wsdl;
    private $username;
    private $pass;
    private $token;
    private $sfec;
    private $connected = false;
    private $em;
    private $sfCappiesID;

    function __construct(ContainerInterface $container, EntityManagerInterface $em)
    {
        $this->wsdl = $container->getParameter("sf_wsdl");
        $this->username = $container->getParameter("sf_username");
        $this->pass = $container->getParameter("sf_password");
        $this->token = $container->getParameter("sf_token");
        $this->sfCappiesID = $container->getParameter('sf_cappies_base_account');
        $this->sfec = new \SforceEnterpriseClient();
        $this->em = $em;
    }

    public function sendSchoolToSalesforce(SeasonalSchool $seasonalSchool){
        $this->_connect();
        //exit;
        $parentChapter = $seasonalSchool->getChapter();
        $chapterSfid = $parentChapter->getSfid();
        if(is_null($chapterSfid)){
            $this->sendChapterToSalesforce($parentChapter);
            $chapterSfid = $parentChapter->getSfid();
        }

        // send the seasonal school
        $schoolSfid = $seasonalSchool->getSfid();
        $school = $seasonalSchool->getSchool();
        if(is_null($schoolSfid)){
            $account = new \stdClass();
            $account->Name = $school->getName();
            $account->ParentId = $chapterSfid;
            $account->Type = 'Member School';
            $account->CIS_School_Code__c = $parentChapter->getDesignation().'-'.$school->getDesignation();
            $account->Phone = $school->getPhone1();
            $account->Last_Active_Season__c = $seasonalSchool->getSeason()->getYear();
            $account->BillingCity = $school->getAddress()->getCity();
            $account->BillingPostalCode = $school->getAddress()->getZip();
            $account->BillingState = $school->getAddress()->getState();
            $account->BillingStreet = $school->getAddress()->getAddressStreet();

            // write the account to SF
            $createResponse = $this->sfec->upsert('CIS_School_Code__c', [$account],'Account');
            $this->handleErrors($createResponse);
            $schoolSfid = $createResponse[0]->id;
            $seasonalSchool->setSfid($createResponse[0]->id);
            $this->em->persist($seasonalSchool);
            $this->em->flush();
        }

        // let's find the school officials and lead critic
        $objSchoolOfficial = null;
        $objLeadCritic= null;
        $participants = $seasonalSchool->getParticipants();

        // send the participants
        $aryIDssToUpdate = [];
        $leadCritic = null;
        $schoolOfficial = null;
        $adviser = null;
        foreach($participants as $participant){
            $contact = new \stdClass();
            $user = $participant->getUser();
            if($user instanceof Critic){
                $contact->Grade_Level__c = $user->getGrade();
            }
            if($participant instanceof SchoolStaff){
                $contact->Phone = $participant->getOfficePhone();
            }
            //$emailAddress = $participant->getUser()->getEmail();
            $contact->FirstName = $user->getFName();
            $contact->LastName = $user->getLName();
            $contact->AccountId = $schoolSfid;
            $contact->Last_Active_Season__c = $seasonalSchool->getSeason()->getYear();
            $contact->School_Role__c = $participant->getGroup()->getName();
            //$contact->CIS_Username__c = substr($participant->getUser()->getUsername(), 0, 20);
            $contact->Email = $participant->getUser()->getEmail();
            $contact->Title = $participant->getGroup()->getName();
            $contact->HomePhone = $participant->getHomePhone();
            $contact->MobilePhone = $participant->getMobilePhone();
            $contact->Last_School_Official__c = null;
            $contact->Last_Lead_Critic__c = null;
            $contact->Last_Advisor__c = null;

            /*dump($contact);
            continue;*/

            // send the create
            $upsertResponse = $this->sfec->upsert('Email', [$contact],'Contact');
            dump($contact);
            dump($upsertResponse);
            $this->handleErrors($upsertResponse);
            $sfid = $upsertResponse[0]->id;

            // we need to capture IDs for later update
            $aryIDssToUpdate[] = $sfid;
            if($participant->getGroup()->getName() == Group::LEAD_CRITIC){
                $leadCritic = $sfid;
            }elseif($participant->getGroup()->getName() == Group::SCHOOL_OFFICIAL){
                $schoolOfficial = $sfid;
            }elseif($participant->getGroup()->getName() == Group::ADVISER){
                $adviser = $sfid;
            }

            // save the sfid to the database
            $participant->setSfid($sfid);
            $this->em->persist($participant);
            $this->em->flush();
        }
        exit;

        // now let's update all the participants with the needed relations
        $aryObjectsToUpdate = [];
        foreach($aryIDssToUpdate as $id){
            $contactToUpdate = new \stdClass();
            $contactToUpdate->Last_School_Official__c = $schoolOfficial;
            $contactToUpdate->Last_Lead_Critic__c = $leadCritic;
            $contactToUpdate->Last_Advisor__c = $adviser;
            $contactToUpdate->Id = $id;
            $aryObjectsToUpdate[] = $contactToUpdate;
        }

        // send the update to SF
        $createResponse = $this->sfec->update($aryObjectsToUpdate,'Contact');
        $this->handleErrors($createResponse);
        echo "hit";exit;
    }

    public function sendChapterToSalesforce(Chapter $chapter){
        $this->_connect();
        if(is_null($chapter->getSfid())){
            // Create Account
            $account = new \stdClass();
            $account->Name = $chapter->getName();
            $account->ParentId = $this->sfCappiesID;
            $account->Type = 'Charter Region';
            $account->CIS_Region__c = $chapter->getDesignation();
            $account->Last_Active_Season__c = $chapter->getSeason()->getYear();
            $account->IRS_EIN_US_Only__c = $chapter->getEin();

            // write the account to SF
            $createResponse = $this->sfec->create(array($account),'Account');
            $this->handleErrors($createResponse);
            $chapter->setSfid($createResponse[0]->id);
            $this->em->persist($chapter);
            $this->em->flush();
        }
        $sfid = $chapter->getSfid();

        // build update statement
        $accountUpdate = new \stdClass();
        $accountUpdate->Id = $sfid;

        foreach($chapter->getBoardMembers() as $boardMember){
            if($boardMember instanceof BoardMember){
                $user = $boardMember->getUser();
                if($user instanceof User){
                    $searchResults = $this->searchForUser($user->getEmail());
                    $contact = new \stdClass();
                    $contact->Last_Active_Season__c = $chapter->getSeason()->getYear();
                    if(count($searchResults) == 1){
                        $userSfid = $searchResults[0];
                        $contact->Id = $userSfid;
                        $createResponse = $this->sfec->update([$contact],'Contact');
                        $this->handleErrors($createResponse);
                        $userSfid = $createResponse[0]->id;
                    }else{
                        $contact->FirstName = $user->getFName();
                        $contact->LastName = $user->getLName();
                        $contact->AccountId = $sfid;
                        $contact->Email = $user->getEmail();
                        $contact->Title = $boardMember->getGroup()->getName();
                        $contact->HomePhone = $boardMember->getHomePhone();
                        $contact->Phone = $boardMember->getOfficePhone();
                        $contact->MobilePhone = $boardMember->getMobilePhone();
                        $createResponse = $this->sfec->create([$contact],'Contact');
                        $this->handleErrors($createResponse);
                        $userSfid = $createResponse[0]->id;
                    }

                    // update the account
                    if($boardMember->getGroup()->getName() == Group::CHAIRMAN){
                        $accountUpdate->Last_Program_Chair__c = $userSfid;
                        $accountUpdate->BillingCity = $boardMember->getAddress()->getCity();
                        $accountUpdate->BillingPostalCode = $boardMember->getAddress()->getZip();
                        $accountUpdate->BillingState = $boardMember->getAddress()->getState();
                        $accountUpdate->BillingStreet = $boardMember->getAddress()->getAddressStreet();
                    }elseif($boardMember->getGroup()->getName() == Group::PROGRAM_DIRECTOR){
                        $accountUpdate->Last_Program_Director__c = $userSfid;
                        $accountUpdate->ShippingCity = $boardMember->getAddress()->getCity();
                        $accountUpdate->ShippingPostalCode = $boardMember->getAddress()->getZip();
                        $accountUpdate->ShippingState = $boardMember->getAddress()->getState();
                        $accountUpdate->ShippingStreet = $boardMember->getAddress()->getAddressStreet();
                    }

                    $boardMember->setSfid($userSfid);
                    $this->em->persist($boardMember);
                    $this->em->flush();
                }
            }
        }
        $updateResult = $this->sfec->update([$accountUpdate],'Account');
        $this->handleErrors($updateResult);

        /*// send the schools
        foreach($chapter->getParticipatingSchools() as $school){
            if($school instanceof SeasonalSchool){
                $sfSchool = new \stdClass();
                $sfSchool->Name = $chapter->getName();
                $sfSchool->ParentId = $chapter->getSfid();
                $sfSchool->Type = 'Member School';
                $sfSchool->BillingCity = $school->getAddress()->getCity();
                $sfSchool->BillingPostalCode = $school->getAddress()->getZip();
                $sfSchool->BillingState = $school->getAddress()->getState();
                $sfSchool->BillingStreet = $school->getAddress()->getAddressStreet();

                $createResponse = $this->sfec->create([$school],'Account');
                $this->handleErrors($createResponse);
                $schoolSfid = $createResponse[0]->id;

                $aryParticipants = $school->getParticipants();
                foreach($aryParticipants as $participant){
                    if($participant instanceof Critic){
                        //todo: finsh
                    }elseif($participant instanceof SchoolStaff){
                        //todo: finsh
                    }
                }
            }
        }



        //dump($createResponse);
        exit;*/
    }

    public function searchForUser($email){
        $this->_connect();
        $result = $this->sfec->search('FIND {'.$email.'} in Email Fields');
        if($result instanceof \SforceSearchResult){
            return $result->searchRecords;
        }
        return [];
        dump($result);exit;
    }

    public function retrieveObject($type, $id){
        $this->_connect();
        $aryFields = $this->describeObjectFields($type);
        return $this->sfec->retrieve(implode(',', $aryFields), $type, [$id]);
    }

    public function describeObjectFields($objectType){
        $this->_connect();
        $describe = $this->sfec->describeSObject('Account');
        $fields = $describe->fields;
        $aryFields = [];
        foreach($fields as $field){
            $aryFields[] = $field->name;
        }
        return $aryFields;
    }

    private function _connect(){
        if(!$this->connected){
            $this->sfec->createConnection(realpath ($this->wsdl));
            $this->sfec->login($this->username, $this->pass.$this->token);
            $this->connected = true;
        }
    }

    private function handleErrors(array $arySfResponse){
        if(property_exists($arySfResponse[0], 'errors')) {
            $aryErrors = [];
            foreach($arySfResponse[0]->errors as $error){
                $aryErrors[] = $error->message.' ('.$error->statusCode.')';
            }
            throw new \Exception(implode('; ', $aryErrors));
        }
    }
}