<?php
namespace AppBundle\Event;

use AppBundle\Entity\Review;
use Symfony\Component\EventDispatcher\Event;

class ReviewEvent extends Event
{
    const STATUS_CHANGED = "chapter.status_changed";

    protected $review;

    public function __construct(Review $review)
    {
        $this->review = $review;
    }

    public function getReview()
    {
        return $this->review;
    }
}