<?php
namespace AppBundle\Event;

use AppBundle\Entity\Chapter;
use Symfony\Component\EventDispatcher\Event;

class ChapterEvent extends Event
{
    const STATUS_CHANGED = "chapter.status_changed";

    protected $chapter;

    public function __construct(Chapter $chapter)
    {
        $this->chapter = $chapter;
    }

    public function getChapter()
    {
        return $this->chapter;
    }
}