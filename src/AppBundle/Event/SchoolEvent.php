<?php

namespace AppBundle\Event;

use AppBundle\Entity\SeasonalSchool;
use Symfony\Component\EventDispatcher\Event;

class SchoolEvent extends Event
{
    const STATUS_CHANGED = "school.status_changed";

    protected $seasonalSchool;

    public function __construct(SeasonalSchool $school)
    {
        $this->seasonalSchool = $school;
    }

    public function getSeasonalSchool()
    {
        return $this->seasonalSchool;
    }

    public function getSchool(){
        return $this->seasonalSchool->getSchool();
    }
}