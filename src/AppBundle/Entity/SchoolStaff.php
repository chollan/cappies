<?php

namespace AppBundle\Entity;

use AppBundle\Entity\MappedSuperclass\SeasonalUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class SchoolStaff
 * @package AppBundle\Entity
 * @ORM\Entity()
 */
class SchoolStaff extends SeasonalUser
{

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Address")
     */
    protected $address;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $officePhone;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isAlsoShowDirector;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $isAlsoMentor;

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getOfficePhone()
    {
        return $this->officePhone;
    }

    /**
     * @param mixed $officePhone
     */
    public function setOfficePhone($officePhone)
    {
        $this->officePhone = $officePhone;
    }

    /**
     * @return mixed
     */
    public function getIsAlsoShowDirector()
    {
        return $this->isAlsoShowDirector;
    }

    /**
     * @param mixed $isAlsoShowDirector
     */
    public function setIsAlsoShowDirector($isAlsoShowDirector)
    {
        $this->isAlsoShowDirector = $isAlsoShowDirector;
    }

    /**
     * @return mixed
     */
    public function getIsAlsoMentor()
    {
        return $this->isAlsoMentor;
    }

    /**
     * @param mixed $isAlsoMentor
     */
    public function setIsAlsoMentor($isAlsoMentor)
    {
        $this->isAlsoMentor = $isAlsoMentor;
    }
}