<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     uniqueConstraints={
 *         @UniqueConstraint(
 *              name="unique_show_critic",
 *              columns={"show_id", "critic_id"}
 *         )
 *     }
 * )
 */
class Review
{
    const STATUS_NOT_STARTED = 0;
    const STATUS_IN_PROGRESS = 1;
    const STATUS_SUBMITTED_TO_MENTOR = 2;
    const STATUS_SUBMITTED_TO_ADMIN = 3;
    const STATUS_REJECTED = 4;
    const STATUS_APPROVED = 5;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SchoolShow", inversedBy="reviews")
     */
    private $show;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Critic")
     */
    private $critic;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ReviewRevision", mappedBy="review", cascade={"persist"})
     */
    private $revisions;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    private $statuses = [
        self::STATUS_NOT_STARTED => 'Not yet started',
        self::STATUS_IN_PROGRESS => 'In Progress',
        self::STATUS_SUBMITTED_TO_MENTOR => 'Submitted to Mentor',
        self::STATUS_SUBMITTED_TO_ADMIN => 'Submitted to Admin',
        self::STATUS_REJECTED => 'Rejected',
        self::STATUS_APPROVED => 'Approved',
    ];

    function __construct(SchoolShow $show, Critic $critic)
    {

        $this->show = $show;
        $this->critic = $critic;
        $this->revisions = new ArrayCollection();
        $this->status = self::STATUS_NOT_STARTED;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getShow()
    {
        return $this->show;
    }

    /**
     * @param mixed $show
     */
    public function setShow($show)
    {
        $this->show = $show;
    }

    /**
     * @return mixed
     */
    public function getCritic()
    {
        return $this->critic;
    }

    /**
     * @param mixed $critic
     */
    public function setCritic($critic)
    {
        $this->critic = $critic;
    }

    /**
     * @return mixed
     */
    public function getRevisions()
    {
        $criteria = Criteria::create()
            ->orderBy(['created' => 'DESC'])
        ;
        return $this->revisions->matching($criteria);
    }

    public function getRevision(){
        //dump($this);exit;
        //return $this->getCurrentRevision();
        $currentRevision = $this->getCurrentRevision();
        $content = null;
        if($currentRevision instanceof ReviewRevision){
            $content = $currentRevision->getContent();
        }
        return $this->createNewRevision(
            $content
        );
    }

    public function setRevision(ReviewRevision $revision){
        if(!is_null($revision->getId())){
            // this is someone modifying an existing record.
            // we dont want that.  lets create a new revision
            $revision = $this->createNewRevision(
                $revision->getContent()
            );
        }
        // only if the content has changed
        $currentRevision = $this->getCurrentRevision();
        if($currentRevision instanceof ReviewRevision){
            if($revision->getContent() == $currentRevision->getContent()){
                return;
            }
        }
        $this->addRevision($revision);
    }

    public function addRevision(ReviewRevision $revision){
        if(!$this->revisions->contains($revision)){
            $this->revisions->add($revision);
            $revision->setReview($this);
        }
    }

    public function getCurrentRevision(){
        $criteria = Criteria::create()
            ->setMaxResults(1)
            ->orderBy(['revisionNo' => 'DESC'])
        ;
        $result = $this->revisions->matching($criteria);
        if(count($result) == 1){
            return $result[0];
        }
        return null;
    }

    public function createNewRevision($content = null){
        $currentRevision = $this->getCurrentRevision();
        $revisionNumber = 1;
        if($currentRevision instanceof ReviewRevision){
            $revisionNumber = $currentRevision->getRevisionNo() + 1;
        }
        $newRevision = new ReviewRevision();
        $newRevision->setContent($content);
        $newRevision->setRevisionNo($revisionNumber);
        $newRevision->setReview($this);
        return $newRevision;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function getStatusString(){
        if(array_key_exists($this->status,$this->statuses)){
            return $this->statuses[$this->status];
        }
        return 'Unknown';
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


}