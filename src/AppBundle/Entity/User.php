<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\Criteria;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table()
 */
class User extends BaseUser
{
    # Old Roles
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN'; #equal to us admin
    const ROLE_ADMIN = 'ROLE_ADMIN'; # grouping of chair, pd, treasurer, manager
    const ROLE_SHOW_DIRECTOR = 'ROLE_SHOW_DIRECTOR';
    const ROLE_CHAPTER_BOARD = 'ROLE_CHAPTER_BOARD';
    const ROLE_CHAIRMAN = 'ROLE_CHAIRMAN';
    const ROLE_TREASURER = 'ROLE_TREASURER';
    const ROLE_STEERING_MEMBER = 'ROLE_STEERING_MEMBER';
    const ROLE_SCHOOL_ADVISER = 'ROLE_SCHOOL_ADVISER';
    const ROLE_SCHOOL_MENTOR = 'ROLE_SCHOOL_MENTOR';
    const ROLE_CRITIC = 'ROLE_CRITIC';
    const ROLE_LEAD_CRITIC = 'ROLE_LEAD_CRITIC';
    //const ROLE_PROCTOR = 'ROLE_PROCTOR';
    //const ROLE_TRUSTEE = 'ROLE_TRUSTEE';
    const ROLE_PROGRAM_MANAGER = 'ROLE_PROGRAM_MANAGER';
    const ROLE_VOTE_ADMIN = 'ROLE_VOTE_ADMIN';
    const ROLE_SCHOOL_ADVISOR = 'ROLE_SCHOOL_ADVISOR';

    # Identifying Roles
    const ROLE_SCHOOL_STAFF = 'ROLE_SCHOOL_STAFF';
    const ROLE_SCHOOL_STUDENT = 'ROLE_SCHOOL_STUDENT';

    # New Roles
    const ROLE_PROGRAM_DIRECTOR = 'ROLE_PROGRAM_DIRECTOR';

    # Grades
    const GRADE_FRESHMAN = 0;
    const GRADE_JUNIOR = 1;
    const GRADE_SOPHOMORE = 2;
    const GRADE_SENIOR = 3;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $namePrefix;

    /**
     * @ORM\Column(type="string")
     */
    protected $fName;

    /**
     * @ORM\Column(type="string")
     */
    protected $lName;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $nameSuffix;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Season")
     * @ORM\JoinColumn(name="season_logged_in_id", referencedColumnName="id", nullable=true)
     */
    protected $seasonLoggedIn;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\MappedSuperclass\SeasonalUser", mappedBy="user")
     */
    protected $activeSeasons;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $mobilePhone;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $homePhone;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $sfid;

    public function __construct(){
        parent::__construct();
        $this->password = uniqid();
    }

    public function __toString(){
        $name = trim($this->namePrefix.' '.$this->fName.' '.$this->lName.' '.$this->nameSuffix);
        if($name != ''){
            /*$position = $this->getCurrentPosition();
            if($position != ''){
                $name .= ' ('.$position.')';
            }*/
            return $name;
        }
        return parent::__toString(); // return username
    }

    /**
     * @return mixed
     */
    public function getFName()
    {
        return $this->fName;
    }

    /**
     * @param mixed $fName
     */
    public function setFName($fName)
    {
        $this->fName = $fName;
    }

    /**
     * @return mixed
     */
    public function getLName()
    {
        return $this->lName;
    }

    /**
     * @param mixed $lName
     */
    public function setLName($lName)
    {
        $this->lName = $lName;
    }

    public function setEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $aryEmailParts = explode('@', $email);
            if($aryEmailParts[0] != ''){
                parent::setUsername($aryEmailParts[0]);
            }
        }
        return parent::setEmail($email);
    }

    /**
     * @return mixed
     */
    public function getSeasonLoggedIn()
    {
        return $this->seasonLoggedIn;
    }

    /**
     * @param mixed $seasonLoggedIn
     */
    public function setSeasonLoggedIn(Season $seasonLoggedIn)
    {
        $this->seasonLoggedIn = $seasonLoggedIn;
    }

    public function isActiveInSeason(Season $season){
        $criteria = Criteria::create()->andWhere(Criteria::expr()->eq('season', $season));
        $activeUser = $this->activeSeasons->matching($criteria);
        if(count($activeUser) == 1){
            return $activeUser[0]->isActive();
        }
        return false;
    }

    public function getRoles()
    {
        if($this->seasonLoggedIn instanceof Season){
            $criteria = Criteria::create()->andWhere(Criteria::expr()->eq('season', $this->seasonLoggedIn));
            $activeUser = $this->activeSeasons->matching($criteria);
            if(count($activeUser) == 1){
                $this->roles = $activeUser[0]->getGroup()->getRoles();
            }
        }
        return parent::getRoles();
    }

    public function getCurrentPosition(){
        $position = $this->getActiveSeason();
        if($position instanceof Critic){
            return 'Critic';
        }elseif($position instanceof BoardMember){
            return "Board Member";
        }elseif($position instanceof SchoolStaff){
            return "School Administrator";
        }
        return "";
    }

    public function getActiveSeason(){
        $return = null;
        if($this->seasonLoggedIn instanceof Season){
            $criteria = Criteria::create()->andWhere(Criteria::expr()->eq('season', $this->seasonLoggedIn));
            $activeUser = $this->activeSeasons->matching($criteria);
            if(count($activeUser) == 1){
                $return = $activeUser[0];
            }
        }
        return $return;
    }

    /**
     * @return mixed
     */
    public function getActiveSeasons()
    {
        return $this->activeSeasons;
    }

    /**
     * @param mixed $activeSeasons
     */
    public function setActiveSeasons($activeSeasons)
    {
        $this->activeSeasons = $activeSeasons;
    }

    /**
     * @return mixed
     */
    public function getMobilePhone()
    {
        return $this->mobilePhone;
    }

    /**
     * @param mixed $mobileNumber
     */
    public function setMobilePhone($mobilePhone)
    {
        $this->mobilePhone = $mobilePhone;
    }

    /**
     * @return mixed
     */
    public function getHomePhone()
    {
        return $this->homePhone;
    }

    /**
     * @param mixed $homePhone
     */
    public function setHomePhone($homePhone)
    {
        $this->homePhone = $homePhone;
    }

    /**
     * @return mixed
     */
    public function getSfid()
    {
        return $this->sfid;
    }

    /**
     * @param mixed $sfid
     */
    public function setSfid($sfid)
    {
        $this->sfid = $sfid;
    }

    /**
     * @return mixed
     */
    public function getNamePrefix()
    {
        return $this->namePrefix;
    }

    /**
     * @param mixed $namePrefix
     */
    public function setNamePrefix($namePrefix)
    {
        $this->namePrefix = $namePrefix;
    }

    /**
     * @return mixed
     */
    public function getNameSuffix()
    {
        return $this->nameSuffix;
    }

    /**
     * @param mixed $nameSuffix
     */
    public function setNameSuffix($nameSuffix)
    {
        $this->nameSuffix = $nameSuffix;
    }

}