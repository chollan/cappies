<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="AppBundle\EntityRepository\ChapterRepository")
 * @ORM\Table(
 *     name="chapter",
 *     uniqueConstraints={
 *         @UniqueConstraint(
 *              name="unique_season_chapter_designation",
 *              columns={"designation", "season_id"}
 *         )
 *     }
 * )
 */
class Chapter
{
    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_REJECTED = 2;

    const TIMEZONE_EASTERN = 5;
    const TIMEZONE_CENTRAL = 6;
    const TIMEZONE_MOUNTAIN = 7;
    const TIMEZONE_PACIFIC = 8;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     * @Assert\Length(
     *      min = 3,
     *      max = 3,
     *      exactMessage="Your chapter designation can only be 3 characters"
     * )
     */
    private $designation;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Assert\Regex(
     *     pattern="/^[1-9]\d?-\d{7}$/",
     *     message="Invalid EIN"
     * )
     */
    private $ein;

    /**
     * @ORM\ManyToOne(targetEntity="Season")
     */
    private $season;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $boundaries;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $distance;

    /**
     * @ORM\Column(type="integer", length=1, nullable=true)
     */
    private $timezone;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $schoolInfo;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\BoardMember", cascade={"persist"})
     */
    private $boardMembers;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\SeasonalSchool", cascade={"persist"}, mappedBy="chapter")
     */
    private $participatingSchools;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     * @Assert\Regex(
     *     pattern="/RA|R/",
     *     message="Program Type must be one of 'RA' or 'R'"
     * )
     */
    private $programType;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     * @Assert\Regex(
     *     pattern="/Gala|Ceremony/",
     *     message="Award Type must be one of 'Gala' or 'Ceremony'"
     * )
     */
    private $awardtype;

    /**
     * @ORM\Column(type="integer", length=2, nullable=true)
     */
    private $minimumReviews;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $fees;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $initSchoolAppDeadline;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $initCriticTrainingDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $midSchoolAppDeadline;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $midCriticTrainingDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $voteDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $awardDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="integer", length=1)
     */
    private $status;

    /**
     * @ORM\Column(type="integer", length=2, nullable=true)
     */
    private $numberOfShows;

    /**
     * @ORM\Column(type="integer", length=2, nullable=true)
     */
    private $reviewsRequiredToVote;

    /**
     * @ORM\Column(type="integer", length=2, nullable=true)
     */
    private $nomPerCat;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $minMeanEvalScore;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $votingDate;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $awardVotingDate;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $votingStartTime;

    /**
     * @ORM\Column(type="time", nullable=true)
     */
    private $votingEndTime;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $votingLocation;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $galaDateTime;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Address", cascade={"persist"})
     */
    private $galaLocation;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $sfid;

    function __construct(){
        $this->programType = 'RA';
        $this->awardtype = 'Gala';
        $this->minimumReviews = 5;
        $this->status = self::STATUS_PENDING;
        $this->numberOfShows = 1;
        $this->boardMembers = new ArrayCollection();
        $this->participatingSchools = new ArrayCollection();
        $this->created = new \DateTime();
        $this->galaLocation = new Address();
    }

    function __toString()
    {
        return (string)$this->name.' ('.$this->designation.')';
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * @param mixed $designation
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
    }

    /**
     * @return mixed
     */
    public function getEin()
    {
        return $this->ein;
    }

    /**
     * @param mixed $ein
     */
    public function setEin($ein)
    {
        $this->ein = $ein;
    }

    /**
     * @return mixed
     */
    public function getSeason()
    {
        return $this->season;
    }

    /**
     * @param mixed $season
     */
    public function setSeason($season)
    {
        $this->season = $season;
    }

    /**
     * @return mixed
     */
    public function getBoundaries()
    {
        return $this->boundaries;
    }

    /**
     * @param mixed $boundaries
     */
    public function setBoundaries($boundaries)
    {
        $this->boundaries = $boundaries;
    }

    /**
     * @return mixed
     */
    public function getDistance()
    {
        return $this->distance;
    }

    /**
     * @param mixed $distance
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;
    }

    /**
     * @return mixed
     */
    public function getSchoolInfo()
    {
        return $this->schoolInfo;
    }

    /**
     * @param mixed $schoolInfo
     */
    public function setSchoolInfo($schoolInfo)
    {
        $this->schoolInfo = $schoolInfo;
    }

    /**
     * @return mixed
     */
    public function getProgramType()
    {
        return $this->programType;
    }

    /**
     * @param mixed $programType
     */
    public function setProgramType($programType)
    {
        $this->programType = $programType;
    }

    /**
     * @return mixed
     */
    public function getAwardtype()
    {
        return $this->awardtype;
    }

    /**
     * @param mixed $awardtype
     */
    public function setAwardtype($awardtype)
    {
        $this->awardtype = $awardtype;
    }

    /**
     * @return mixed
     */
    public function getMinimumReviews()
    {
        return $this->minimumReviews;
    }

    /**
     * @param mixed $minmumReviews
     */
    public function setMinimumReviews($minimumReviews)
    {
        $this->minimumReviews = $minimumReviews;
    }

    /**
     * @return mixed
     */
    public function getFees()
    {
        return $this->fees;
    }

    /**
     * @param mixed $fees
     */
    public function setFees($fees)
    {
        $this->fees = $fees;
    }

    /**
     * @return mixed
     */
    public function getInitSchoolAppDeadline()
    {
        return $this->initSchoolAppDeadline;
    }

    /**
     * @param mixed $initSchoolAppDeadline
     */
    public function setInitSchoolAppDeadline($initSchoolAppDeadline)
    {
        $this->initSchoolAppDeadline = $initSchoolAppDeadline;
    }

    /**
     * @return mixed
     */
    public function getInitCriticTrainingDate()
    {
        return $this->initCriticTrainingDate;
    }

    /**
     * @param mixed $initCriticTrainingDate
     */
    public function setInitCriticTrainingDate($initCriticTrainingDate)
    {
        $this->initCriticTrainingDate = $initCriticTrainingDate;
    }

    /**
     * @return mixed
     */
    public function getMidSchoolAppDeadline()
    {
        return $this->midSchoolAppDeadline;
    }

    /**
     * @param mixed $midSchoolAppDeadline
     */
    public function setMidSchoolAppDeadline($midSchoolAppDeadline)
    {
        $this->midSchoolAppDeadline = $midSchoolAppDeadline;
    }

    /**
     * @return mixed
     */
    public function getMidCriticTrainingDate()
    {
        return $this->midCriticTrainingDate;
    }

    /**
     * @param mixed $midCriticTrainingDate
     */
    public function setMidCriticTrainingDate($midCriticTrainingDate)
    {
        $this->midCriticTrainingDate = $midCriticTrainingDate;
    }

    /**
     * @return mixed
     */
    public function getVoteDate()
    {
        return $this->voteDate;
    }

    /**
     * @param mixed $voteDate
     */
    public function setVoteDate($voteDate)
    {
        $this->voteDate = $voteDate;
    }

    /**
     * @return mixed
     */
    public function getAwardDate()
    {
        return $this->awardDate;
    }

    /**
     * @param mixed $awardDate
     */
    public function setAwardDate($awardDate)
    {
        $this->awardDate = $awardDate;
    }

    /**
     * @return mixed
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param mixed $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getNumberOfShows()
    {
        return $this->numberOfShows;
    }

    /**
     * @param mixed $numberOfShows
     */
    public function setNumberOfShows($numberOfShows)
    {
        $this->numberOfShows = $numberOfShows;
    }

    /**
     * @return mixed
     */
    public function getReviewsRequiredToVote()
    {
        return $this->reviewsRequiredToVote;
    }

    /**
     * @param mixed $reviewsRequiredToVote
     */
    public function setReviewsRequiredToVote($reviewsRequiredToVote)
    {
        $this->reviewsRequiredToVote = $reviewsRequiredToVote;
    }

    /**
     * @return mixed
     */
    public function getNomPerCat()
    {
        return $this->nomPerCat;
    }

    /**
     * @param mixed $nomPerCat
     */
    public function setNomPerCat($nomPerCat)
    {
        $this->nomPerCat = $nomPerCat;
    }

    /**
     * @return mixed
     */
    public function getMinMeanEvalScore()
    {
        return $this->minMeanEvalScore;
    }

    /**
     * @param mixed $minMeanEvalScore
     */
    public function setMinMeanEvalScore($minMeanEvalScore)
    {
        $this->minMeanEvalScore = $minMeanEvalScore;
    }

    /**
     * @return mixed
     */
    public function getVotingDate()
    {
        return $this->votingDate;
    }

    /**
     * @param mixed $votingDate
     */
    public function setVotingDate($votingDate)
    {
        $this->votingDate = $votingDate;
    }

    /**
     * @return mixed
     */
    public function getAwardVotingDate()
    {
        return $this->awardVotingDate;
    }

    /**
     * @param mixed $awardVotingDate
     */
    public function setAwardVotingDate($awardVotingDate)
    {
        $this->awardVotingDate = $awardVotingDate;
    }

    /**
     * @return mixed
     */
    public function getVotingStartTime()
    {
        return $this->votingStartTime;
    }

    /**
     * @param mixed $votingStartTime
     */
    public function setVotingStartTime($votingStartTime)
    {
        $this->votingStartTime = $votingStartTime;
    }

    /**
     * @return mixed
     */
    public function getVotingEndTime()
    {
        return $this->votingEndTime;
    }

    /**
     * @param mixed $votingEndTime
     */
    public function setVotingEndTime($votingEndTime)
    {
        $this->votingEndTime = $votingEndTime;
    }

    /**
     * @return mixed
     */
    public function getVotingLocation()
    {
        return $this->votingLocation;
    }

    /**
     * @param mixed $votingLocation
     */
    public function setVotingLocation($votingLocation)
    {
        $this->votingLocation = $votingLocation;
    }

    /**
     * @return mixed
     */
    public function getGalaDateTime()
    {
        return $this->galaDateTime;
    }

    /**
     * @param mixed $galaDateTime
     */
    public function setGalaDateTime($galaDateTime)
    {
        $this->galaDateTime = $galaDateTime;
    }

    /**
     * @return mixed
     */
    public function getGalaLocation()
    {
        return $this->galaLocation;
    }

    /**
     * @param mixed $galaLocation
     */
    public function setGalaLocation(Address $galaLocation)
    {
        $this->galaLocation = $galaLocation;
    }

    /**
     * @return mixed
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * @param mixed $timezone
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
    }

    public function addBoardMember(BoardMember $member){
        if(!$this->boardMembers->contains($member)){
            $this->boardMembers->add($member);
        }
    }

    public function removeBoardMember(BoardMember $member){
        if($this->boardMembers->contains($member)){
            $this->boardMembers->removeElement($member);
        }
    }

    /**
     * @return mixed
     */
    public function getBoardMembers()
    {
        return $this->boardMembers;
    }

    public function addParticipatingSchool(SeasonalSchool $school)
    {
        if(!$this->participatingSchools->contains($school)){
            $this->participatingSchools->add($school);
            $school->setChapter($this);
        }
    }

    public function removeParticipatingSchool(SeasonalSchool $school)
    {
        if($this->participatingSchools->contains($school)){
            $this->participatingSchools->removeElement($school);
        }
    }

    /**
     * @return mixed
     */
    public function getParticipatingSchools()
    {
        return $this->participatingSchools;
    }

    public function getTimezoneString(){
        switch($this->timezone){
            case self::TIMEZONE_EASTERN:
                return "Eastern";
            case self::TIMEZONE_CENTRAL:
                return "Central";
            case self::TIMEZONE_MOUNTAIN:
                return "Mountain";
            case self::TIMEZONE_PACIFIC:
                return "Pacific";
        }
    }

    /**
     * @return mixed
     */
    public function getSfid()
    {
        return $this->sfid;
    }

    /**
     * @param mixed $sfid
     */
    public function setSfid($sfid)
    {
        $this->sfid = $sfid;
    }



    /**
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context, $payload){
        // let's validate the
        if(count($this->getBoardMembers()) > 12){
            $context->buildViolation('You are only able to have 12 board members')->atPath('boardMembers')->addViolation();
        }
        if(count($this->getBoardMembers()) < 4){
            $context->buildViolation('You must have more than 4 board members')->atPath('boardMembers')->addViolation();
        }
        $aryExistingBoardMembers = [];
        $aryRequiredMembers = ['Program Director', 'Chairman', 'Treasurer', 'Steering Committee Member'];
        foreach($this->getBoardMembers() as $boardMember){
            $aryExistingBoardMembers[] = $boardMember->getGroup()->getName();
        }
        $aryMissing = array_diff($aryRequiredMembers,$aryExistingBoardMembers);
        if(count($aryMissing) > 0){
            $context->buildViolation('You\'re missing the following Board Members: '.implode(', ', $aryMissing))->atPath('boardMembers')->addViolation();
        }
        if(count($this->getParticipatingSchools()) < 4){
            $context->buildViolation('You must have 4 schools participating')->atPath('participatingSchools')->addViolation();
        }
    }
}