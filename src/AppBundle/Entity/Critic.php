<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\MappedSuperclass\SeasonalUser;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Critic
 * @package AppBundle\Entity
 * @ORM\Entity(repositoryClass="AppBundle\EntityRepository\CriticRepository")
 */
class Critic extends SeasonalUser
{


    /**
     * @ORM\Column(type="string", nullable=true, length=1)
     * @Assert\Regex(
     *     pattern="/[MF]{1}/",
     *     message="Must contain M or F only"
     * )
     */
    protected $gender;

    /**
     * @ORM\Column(type="string", nullable=true, length=50)
     */
    protected $grade;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\CriticTeam", inversedBy="members")
     */
    protected $criticTeam;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\CriticDeclinedShow", mappedBy="critic")
     */
    protected $declinedShows;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\CriticVolunteer", mappedBy="critic")
     */
    protected $volunteerShows;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $notificationEmail;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $notificationSms;

    function __construct()
    {
        $this->declinedShows = new ArrayCollection();
    }


    /**
     * @return mixed
     */
    public function getGender()
    {
        switch($this->gender){
            case 'F':
                return "Female";
            case 'M':
                return "Male";
        }
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getGrade()
    {
        switch($this->grade){
            case User::GRADE_FRESHMAN:
                return "Freshman";
            case User::GRADE_JUNIOR:
                return "Junior";
            case User::GRADE_SOPHOMORE:
                return "Sophmore";
            case User::GRADE_SENIOR:
                return "Senior";
        }
        return $this->grade;
    }

    /**
     * @param mixed $grade
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;
    }

    /**
     * @return mixed
     */
    public function getCriticTeam()
    {
        return $this->criticTeam;
    }

    /**
     * @param mixed $criticTeam
     */
    public function setCriticTeam(CriticTeam $criticTeam)
    {
        $this->criticTeam = $criticTeam;
    }

    /**
     * @return mixed
     */
    public function getDeclinedShows()
    {
        return $this->declinedShows;
    }

    public function addDeclinedShow(CriticDeclinedShow $declinedShow){
        if(!$this->declinedShows->contains($declinedShow)){
            $this->declinedShows->add($declinedShow);
        }
    }

    public function removeDeclinedShow(CriticDeclinedShow $declinedShow){
        if($this->declinedShows->contains($declinedShow)){
            $this->declinedShows->removeElement($declinedShow);
        }
    }

    /**
     * @return mixed
     */
    public function getNotificationEmail()
    {
        return $this->notificationEmail;
    }

    /**
     * @param mixed $notificationEmail
     */
    public function setNotificationEmail($notificationEmail)
    {
        $this->notificationEmail = $notificationEmail;
    }

    /**
     * @return mixed
     */
    public function getNotificationSms()
    {
        return $this->notificationSms;
    }

    /**
     * @param mixed $notificationSms
     */
    public function setNotificationSms($notificationSms)
    {
        $this->notificationSms = $notificationSms;
    }


}