<?php
namespace AppBundle\Entity;

use FOS\UserBundle\Model\Group as BaseGroup;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_group")
 */
class Group extends BaseGroup
{

    const SUPER_ADMIN = "Super Admin";
    const ADVISER = "Adviser";
    const SHOW_DIRECTOR = "Show Director";
    const MENTOR = "Mentor";
    const CHAIRMAN = "Chair";
    const PROGRAM_DIRECTOR = "Program Director";
    const TREASURER = "Treasurer";
    const PROGRAM_MANAGER = "Program Manager";
    const STEERING_COMMITTEE_MEMBER = "Steering Committee Member";
    const CRITIC = "Critic";
    const LEAD_CRITIC = "Lead Critic";
    const BOOSTER = "Booster";
    const ACCOUNTS_PAYABLE = "Accounts Payable";
    const SCHOOL_OFFICIAL = "School Official";

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $allowedToLogin;

    function __construct($name, array $roles = array(), $bAllowedToLogin = true)
    {
        $this->allowedToLogin = $bAllowedToLogin;
        parent::__construct($name, $roles);
    }

    function __toString(){
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function isAllowedToLogin()
    {
        return $this->allowedToLogin;
    }

    /**
     * @param mixed $allowedToLogin
     */
    public function setAllowedToLogin($allowedToLogin)
    {
        $this->allowedToLogin = $allowedToLogin;
    }


}