<?php
namespace AppBundle\Entity\MappedSuperclass;

use AppBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;


/**
 * @ORM\Entity
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\MappedSuperclass()
 * @ORM\Table(
 *     name="seasonal_user",
 *     uniqueConstraints={
 *         @UniqueConstraint(
 *              name="unique_user",
 *              columns={"user_id", "group_id", "season_id"}
 *         )
 *     }
 * )
 */
class SeasonalUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="activeSeasons", cascade={"persist"})
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Group")
     */
    protected $group;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Season")
     */
    protected $season;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $active;

    function __construct()
    {
        $this->active = true;
    }

    function __toString(){
        return (string)$this->getUser();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param mixed $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }

    /**
     * @return mixed
     */
    public function getSeason()
    {
        return $this->season;
    }

    /**
     * @param mixed $season
     */
    public function setSeason($season)
    {
        $this->season = $season;
    }

    /**
     * @return mixed
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param mixed $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    public function setMobilePhone($mobilePhone){
        if($this->user instanceof User){
            $this->user->setMobilePhone($mobilePhone);
        }
    }

    public function getMobilePhone(){
        if($this->user instanceof User){
            return $this->user->getMobilePhone();
        }
    }

    public function setHomePhone($homePhone){
        if($this->user instanceof User){
            $this->user->setMobilePhone($homePhone);
        }
    }

    public function getHomePhone(){
        if($this->user instanceof User){
            return $this->user->getMobilePhone();
        }
    }

    public function setSfid($sfid){
        if($this->user instanceof User){
            $this->user->setSfid($sfid);
        }
    }

    public function getSfid(){
        if($this->user instanceof User){
            return $this->user->getSfid();
        }
    }

    public function getEmail(){
        if($this->user instanceof User){
            return $this->user->getEmail();
        }
    }
}