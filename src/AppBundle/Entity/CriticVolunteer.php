<?php
namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table
 */
class CriticVolunteer
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SchoolShow", inversedBy="volunteerCritics")
     */
    private $show;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Critic", inversedBy="volunteerShows")
     */
    private $critic;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    function __construct(Critic $critic, SchoolShow $show)
    {
        $this->critic = $critic;
        $this->show = $show;
        $this->date = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getShow()
    {
        return $this->show;
    }

    /**
     * @param mixed $show
     */
    public function setShow(SchoolShow $show)
    {
        $this->show = $show;
    }

    /**
     * @return mixed
     */
    public function getCritic()
    {
        return $this->critic;
    }

    /**
     * @param mixed $critic
     */
    public function setCritic(Critic $critic)
    {
        $this->critic = $critic;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }


}