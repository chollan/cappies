<?php
namespace AppBundle\Entity;

use AppBundle\Entity\MappedSuperclass\SeasonalUser;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class SeasonalSchool
 * @package AppBundle\Entity
 * @ORM\Entity(repositoryClass="AppBundle\EntityRepository\SeasonalSchoolRepository")
 * @ORM\Table(
 *     name="seasonal_school",
 *     uniqueConstraints={
 *         @UniqueConstraint(
 *              name="unique_school_season",
 *              columns={"school_id", "season_id"}
 *         )
 *     }
 * )
 */
class SeasonalSchool
{
    const STATUS_PENDING = 0;
    const STATUS_APPROVED = 1;
    const STATUS_REJECTED = 2;
    const STATUS_APPROVED_TO_APPLY = 3;
    const STATUS_PENDING_CHAPTER = 4;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\School", cascade={"persist"})
     */
    private $school;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Season")
     */
    private $season;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\MappedSuperclass\SeasonalUser", cascade={"persist"})
     */
    private $participants;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Chapter", inversedBy="participatingSchools")
     */
    private $chapter;

    /**
     * @ORM\Column(type="integer", length=1, nullable=true)
     */
    private $participationLevel;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\SchoolShow", mappedBy="school", cascade={"persist"})
     */
    private $shows;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $newspaper;

    /**
     * @ORM\Column(type="integer", length=1, nullable=true)
     */
    private $newspaperPublishStatus;

    /**
     * @ORM\Column(type="array")
     */
    private $criticsUnavailableDates;

    /**
     * @ORM\Column(type="integer", length=1)
     */
    private $status;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $applicationLink;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $sfid;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\CriticTeam", mappedBy="school")
     */
    private $criticTeam;

    private $contacts;

    private $critics;

    function __construct(){
        $this->participants = new ArrayCollection();
        $this->shows = new ArrayCollection();
        $this->contacts = new ArrayCollection();
        $this->critics = new ArrayCollection();
        $this->criticsUnavailableDates = new ArrayCollection();
        $this->status = self::STATUS_PENDING_CHAPTER;
    }

    function __toString(){
        return (string)$this->getSchool()->getName();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * @param mixed $school
     */
    public function setSchool(School $school)
    {
        $this->school = $school;
    }

    /**
     * @return mixed
     */
    public function getSeason()
    {
        return $this->season;
    }

    /**
     * @param mixed $season
     */
    public function setSeason(Season $season)
    {
        $this->season = $season;
    }

    /**
     * @param SeasonalUser $user
     */
    public function addParticipant(SeasonalUser $user)
    {
        if(!$this->participants->contains($user)){
            $this->participants->add($user);
        }
    }

    /**
     * @param mixed $participants
     */
    public function removeParticipant(SeasonalUser $user)
    {
        if($this->participants->contains($user)){
            $this->participants->removeElement($user);
        }
    }

    public function getParticipants(){
        return $this->participants;
    }

    public function getParticipantInGroup(Group $group){
        $return = $this->participants->filter(function (SeasonalUser $participant) use ($group){
            return $participant->getGroup() == $group;
        });
        if(count($return) == 1){
            return $return[0];
        }
        return $return->toArray();
    }

    /**
     * @return mixed
     */
    public function getChapter()
    {
        return $this->chapter;
    }

    /**
     * @param mixed $chapter
     */
    public function setChapter(Chapter $chapter)
    {
        $this->chapter = $chapter;
        $chapter->addParticipatingSchool($this);
    }

    /**
     * @return mixed
     */
    public function getParticipationLevel()
    {
        return $this->participationLevel;
    }

    /**
     * @param mixed $participationLevel
     */
    public function setParticipationLevel($participationLevel)
    {
        $this->participationLevel = $participationLevel;
    }

    /**
     * @return mixed
     */
    public function getShows()
    {
        return $this->shows;
    }

    public function addShow(SchoolShow $show){
        if(!$this->shows->contains($show)){
            $this->shows->add($show);
        }
    }

    public function removeShow(SchoolShow $show){
        if($this->shows->contains($show)){
            $this->shows->removeElement($show);
        }
    }

    /**
     * @return mixed
     */
    public function getNewspaper()
    {
        return $this->newspaper;
    }

    /**
     * @param mixed $newspaper
     */
    public function setNewspaper($newspaper)
    {
        $this->newspaper = $newspaper;
    }

    /**
     * @return mixed
     */
    public function getNewspaperPublishStatus()
    {
        switch($this->newspaperPublishStatus){
            case School::NEWSPAPER_PUBLISH_TWO:
                return 'Has agreed to publish two Cappies reviews of each Cappies Show at our school.';
            case School::NEWSPAPER_PUBLISH_ONE:
                return  'Has agreed to publish one Cappies review of each Cappies Show at our school.';
            case School::NEWSPAPER_PUBLISH_UNDECIDED:
                return 'Has not decided whether to publish any Cappies reviews.';
            case School::NEWSPAPER_PUBLISH_REFUSED:
                return 'Has refused to publish any Cappies reviews.';
            case School::NEWSPAPER_PUBLISH_NO_PAPER:
                return 'Our school has no School Newspaper.';
        }
        return $this->newspaperPublishStatus;
    }

    /**
     * @param mixed $newspaperPublishStatus
     */
    public function setNewspaperPublishStatus($newspaperPublishStatus)
    {
        $this->newspaperPublishStatus = $newspaperPublishStatus;
    }

    /**
     * @return mixed
     */
    public function getCriticsUnavailableDates()
    {
        return $this->criticsUnavailableDates;
    }

    public function addCriticsUnavailableDate(\DateTime $dateTime){
        if(!$this->criticsUnavailableDates->contains($dateTime)){
            $this->criticsUnavailableDates->add($dateTime);
        }
    }

    public function removeCriticsUnavailableDate(\DateTime $dateTime){
        if($this->criticsUnavailableDates->contains($dateTime)){
            $this->criticsUnavailableDates->removeElement($dateTime);
        }
    }

    public function addContact(SchoolStaff $contact){
        if(!($this->contacts instanceof ArrayCollection)){
            $this->contacts = new ArrayCollection();
        }
        if(!$this->contacts->contains($contact)){
            $this->contacts->add($contact);
        }
    }

    public function removeContact(SchoolStaff $contact){
        if($this->contacts->contains($contact)){
            $this->contacts->removeElement($contact);
        }
    }

    public function addCritic(Critic $critic){
        if(!($this->critics instanceof ArrayCollection)){
            $this->critics = new ArrayCollection();
        }
        if(!$this->critics->contains($critic)){
            $this->critics->add($critic);
        }
    }

    public function removeCritic(Critic $critic){
        if($this->critics->contains($critic)){
            $this->critics->removeElement($critic);
        }
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getApplicationLink()
    {
        return $this->applicationLink;
    }

    /**
     * @param mixed $applicationLink
     */
    public function setApplicationLink($applicationLink)
    {
        $this->applicationLink = $applicationLink;
    }

    /**
     * @return ArrayCollection
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * @return ArrayCollection
     */
    public function getCritics()
    {
        if(is_null($this->critics)){
            $return = new ArrayCollection();
            foreach($this->getParticipants() as $participant){
                if($participant instanceof Critic){
                    $return->add($participant);
                }
            }
            return $return;
        }
        return $this->critics;
    }

    public function getSchoolStaff(){
        $return = new ArrayCollection();
        foreach($this->getParticipants() as $participant){
            if($participant instanceof SchoolStaff){
                $return->add($participant);
            }
        }
        return $return;
    }

    /**
     * @return mixed
     */
    public function getSfid()
    {
        return $this->sfid;
    }

    /**
     * @param mixed $sfid
     */
    public function setSfid($sfid)
    {
        $this->sfid = $sfid;
    }

    /**
     * @return mixed
     */
    public function getCriticTeam()
    {
        return $this->criticTeam;
    }


    /**
     * @Assert\Callback()
     */
    public function validate(ExecutionContextInterface $context, $payload){
        /*school contact needs
        full = 1 person in all 3 rolls
        partial = 1 person in all 3 rolls
        limited = advisor/mentor only*/

        // validate number of Shows
        if($this->chapter instanceof Chapter){
            $numberOfShows = $this->chapter->getNumberOfShows();
            if($numberOfShows > count($this->shows)){
                $context->buildViolation('You are required to have '.$numberOfShows.' or more shows.  Please add more shows')->atPath('shows')->addViolation();
            }
        }

        // validate that there is atleast an adviser in the contacts
        $hasAdviser = false;
        foreach($this->contacts as $contact){
            if($contact->getGroup()->getName() == "Adviser"){
                $hasAdviser = true;
                break;
            }
        }
        if(!$hasAdviser){
            $context->buildViolation('You are required to have a school adviser.  Please add one.')->atPath('contacts')->addViolation();
        }

        // validate that we have a lead critic
        $hasLeadCritic = false;
        if(!is_null($this->critics)){
            foreach($this->critics as $critic){
                if($critic->getGroup()->getName() == "Lead Critic"){
                    $hasLeadCritic = true;
                    break;
                }
            }
        }
        if(!$hasLeadCritic){
            $context->buildViolation('You are required to have a Lead Critic.  Please add one.')->atPath('critics')->addViolation();
        }
    }


}