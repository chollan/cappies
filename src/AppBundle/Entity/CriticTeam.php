<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity(repositoryClass="AppBundle\EntityRepository\CriticTeamRepository")
 * @ORM\Table(name="critic_team")
 */
class CriticTeam
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\SeasonalSchool", inversedBy="criticTeam")
     * @ORM\JoinColumn(nullable=true)
     */
    private $school;

    /**
     * @ORM\Column(type="boolean")
     */
    private $regional;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Critic", mappedBy="criticTeam")
     */
    private $members;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\CriticTeamShowAssignment", mappedBy="team")
     */
    private $assignments;

    function __construct(){
        $this->members = new ArrayCollection();
    }

    function __toString()
    {
        return (string)$this->name;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * @param mixed $school
     */
    public function setSchool(SeasonalSchool $school)
    {
        $this->school = $school;
    }

    /**
     * @return mixed
     */
    public function getRegional()
    {
        return $this->regional;
    }

    /**
     * @param mixed $regional
     */
    public function setRegional($regional)
    {
        $this->regional = $regional;
    }

    public function getMembers(){
        return $this->members;
    }

    public function addMember(Critic $critic){
        if(!$this->members->contains($critic)){
            $this->members->add($critic);
            $critic->setCriticTeam($this);
        }
    }

    public function removeMember(Critic $critic){
        if($this->members->contains($critic)){
            $this->members->removeElement($critic);
            //TODO: Confirm that this works
            $critic->setCriticTeam(null);
        }
    }

    public function getAssignments(){
        return $this->assignments;
    }
}