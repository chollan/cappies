<?php
namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @ORM\Entity(repositoryClass="AppBundle\EntityRepository\SchoolShowRepository")
 * @ORM\Table(name="school_show")
 */
class SchoolShow
{
    const TYPE_MUSICAL = "M";
    const TYPE_PLAY = 'P';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Address",cascade={"persist"})
     */
    private $address;

    /**
     * @ORM\Column(type="string")
     */
    private $addressSameAsSchool;

    /**
     * @ORM\Column(type="array")
     */
    private $dates;

    /**
     * @ORM\Column(type="datetime")
     */
    private $selectedDate;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $reviewDeadline;

    /**
     * @ORM\Column(type="integer")
     */
    private $criticLimit;

    /**
     * @ORM\Column(type="array")
     */
    private $dressRehearsalDates;

    /**
     * @ORM\Column(type="string")
     */
    private $reviewDressRehearsal;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SeasonalSchool", inversedBy="shows")
     */
    private $school;

    /**
     * @ORM\Column(type="boolean")
     */
    private $cappiesShow;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\CriticVolunteer", mappedBy="show")
     */
    private $volunteerCritics;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Review", mappedBy="show")
     */
    private $reviews;

    function __toString()
    {
        return (string)$this->name;
    }

    function __construct(){
        $this->dressRehearsalDates = new ArrayCollection();
        $this->dates = new ArrayCollection();
        $this->reviewDressRehearsal = false;
        $this->addressSameAsSchool = true;
        $this->criticLimit = 0;
        $this->reviews = new ArrayCollection();
        //$this->address = new Address();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        switch($this->type){
            case "M":
                return "Musical";
            case 'P':
                return 'Play';
        }
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        if($this->addressSameAsSchool){
            if($this->school instanceof SeasonalSchool){
                return $this->getSchool()->getSchool()->getAddress();
            }
        }
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress(Address $address = null)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getAddressSameAsSchool()
    {
        return $this->addressSameAsSchool;
    }

    /**
     * @param mixed $addressSameAsSchool
     */
    public function setAddressSameAsSchool($addressSameAsSchool)
    {
        $this->addressSameAsSchool = $addressSameAsSchool;
    }

    /**
     * @return mixed
     */
    public function getDates()
    {
        return $this->dates;
    }

    /**
     * @param mixed $dates
     */
    /*public function setDates(ArrayCollection $dates)
    {
        $this->dates = $dates;
    }*/

    public function addDate(\DateTime $date){
        if(!$this->dates->contains($date)){
            if($this->dates->count() == 0){
                $this->setSelectedDate($date);
            }
            $this->dates->add($date);
        }
    }

    public function removeDate(\DateTime $date){
        if($this->dates->contains($date)){
            $this->dates->removeElement($date);
        }
    }

    /**
     * @return mixed
     */
    public function getDressRehearsalDates()
    {
        return $this->dressRehearsalDates;
    }

    /**
     * @param mixed $dressRehearsalDates
     */
    public function setDressRehearsalDates($dressRehearsalDates)
    {
        $this->dressRehearsalDates = $dressRehearsalDates;
    }

    /**
     * @return mixed
     */
    public function getReviewDressRehearsal()
    {
        return $this->reviewDressRehearsal;
    }

    /**
     * @param mixed $reviewDressRehearsal
     */
    public function setReviewDressRehearsal($reviewDressRehearsal)
    {
        $this->reviewDressRehearsal = $reviewDressRehearsal;
    }

    /**
     * @return mixed
     */
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * @param mixed $school
     */
    public function setSchool(SeasonalSchool $school)
    {
        $this->school = $school;
    }

    /**
     * @return mixed
     */
    public function getSelectedDate()
    {
        return $this->selectedDate;
    }

    /**
     * @param mixed $selectedDate
     */
    public function setSelectedDate(\DateTime $selectedDate)
    {
        $deadline = clone $selectedDate;
        $dow = $selectedDate->format('N');
        if($dow <= 5){
            // on or before friday = 10AM sunday
            $add = 7 - $dow;
            $interval = new \DateInterval('P'.$add.'D');
            $deadline->add($interval);
            $deadline->setTime(10, 0);
        }elseif ($dow == 6){
            $deadline->add(new \DateInterval('P1D'));
            if($selectedDate->format('G') > 17){
                // before 5 pm is afternoon
                // saturday afternoon = 12PM sunday
                $deadline->setTime(12,0);
            }else{
                // after 5 pm is evenint
                // saturday evening = 2PM sunday
                $deadline->setTime(14,0);
            }
        }elseif ($dow == 7){
            // sunday evening = 9PM sunday
            $deadline->setTime(21,0);
        }
        $this->set($deadline);
        $this->selectedDate = $selectedDate;
        exit;
    }

    /**
     * @return mixed
     */
    public function getCappiesShow()
    {
        return $this->cappiesShow;
    }

    /**
     * @param mixed $cappiesShow
     */
    public function setCappiesShow($cappiesShow)
    {
        $this->cappiesShow = $cappiesShow;
    }

    /**
     * @return mixed
     */
    public function getReviewDeadline()
    {
        return $this->reviewDeadline;
    }

    /**
     * @param mixed $reviewDeadline
     */
    public function setReviewDeadline(\DateTime $reviewDeadline)
    {
        $this->reviewDeadline = $reviewDeadline;
    }

    /**
     * @return mixed
     */
    public function getCriticLimit()
    {
        return $this->criticLimit;
    }

    /**
     * @param mixed $criticLimit
     */
    public function setCriticLimit($criticLimit)
    {
        $this->criticLimit = $criticLimit;
    }

    public function getVolunteerCritics()
    {
        return $this->volunteerCritics;
    }

    public function getReviews(Critic $critic = null){
        $criteria = Criteria::create();
        if($critic instanceof Critic){
            $criteria
                ->andWhere(
                    Criteria::expr()->eq('critic', $critic)
                )
            ;
            return $this->reviews->matching($criteria)[0];
        }
        return $this->reviews->matching($criteria);
    }

    public function getReviewStatusForCritic(Critic $critic){
        $review = $this->getReviews($critic);
        if(count($review) == 1){
            return $review[0]->getStatus();
        }
        return null;
    }

    public function getReviewStatusStringForCritic(Critic $critic){
        $review = $this->getReviews($critic);
        if(count($review) == 1){
            return $review[0]->getStatusString();
        }
        return null;
    }
}