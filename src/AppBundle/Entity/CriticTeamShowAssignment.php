<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\EntityRepository\CriticTeamShowAssignmentRepository")
 * @ORM\Table
 */
class CriticTeamShowAssignment
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\CriticTeam", inversedBy="assignments")
     */
    private $team;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\SchoolShow")
     */
    private $shows;

    /**
     * @ORM\Column(type="datetime")
     */
    private $assigned;

    function __construct(CriticTeam $team)
    {
        $this->assigned = new \DateTime();
        $this->shows = new ArrayCollection();
        $this->team = $team;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @param mixed $team
     */
    public function setTeam($team)
    {
        $this->team = $team;
    }

    /**
     * @return mixed
     */
    public function getShows()
    {
        return $this->shows;
    }

    public function addShow(SchoolShow $show){
        if(!$this->shows->contains($show)){
            $this->shows->add($show);
        }
    }

    public function removeShow(SchoolShow $show){
        if($this->shows->contains($show)){
            $this->shows->removeElement($show);
        }
    }

    /**
     * @return mixed
     */
    public function getAssigned()
    {
        return $this->assigned;
    }



}