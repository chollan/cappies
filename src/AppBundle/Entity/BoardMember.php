<?php
namespace AppBundle\Entity;

use AppBundle\Entity\MappedSuperclass\SeasonalUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class BoardMember
 * @package AppBundle\Entity
 * @ORM\Entity()
 */
class BoardMember extends SeasonalUser {

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Address", cascade={"persist"})
     */
    protected $address;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $officePhone;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $notificationEmail;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $notificationSms;

    function __construct()
    {
        $this->user = new User();
        //$this->address = new Address();
        parent::__construct();
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getOfficePhone()
    {
        return $this->officePhone;
    }

    /**
     * @param mixed $officePhone
     */
    public function setOfficePhone($officePhone)
    {
        $this->officePhone = $officePhone;
    }

    /**
     * @return mixed
     */
    public function getNotificationEmail()
    {
        return $this->notificationEmail;
    }

    /**
     * @param mixed $notificationEmail
     */
    public function setNotificationEmail($notificationEmail)
    {
        $this->notificationEmail = $notificationEmail;
    }

    /**
     * @return mixed
     */
    public function getNotificationSms()
    {
        return $this->notificationSms;
    }

    /**
     * @param mixed $notificationSms
     */
    public function setNotificationSms($notificationSms)
    {
        $this->notificationSms = $notificationSms;
    }


}