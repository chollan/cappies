<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table
 */
class CriticDeclinedShow
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Critic", inversedBy="declinedShows")
     * @ORM\JoinColumn(nullable=false)
     */
    private $critic;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SchoolShow")
     * @ORM\JoinColumn(nullable=false)
     */
    private $show;

    /**
     * @ORM\Column(type="datetime")
     */
    private $declinedDate;

    function __construct(Critic $critic = null, SchoolShow $show = null)
    {
        if(!is_null($critic)){
            $this->critic = $critic;
            $critic->addDeclinedShow($this);
        }
        if(!is_null($show)){
            $this->show = $show;
        }
        $this->declinedDate = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCritic()
    {
        return $this->critic;
    }

    /**
     * @param mixed $critic
     */
    public function setCritic(Critic $critic)
    {
        $this->critic = $critic;
        $critic->addDeclinedShow($this);
    }

    /**
     * @return mixed
     */
    public function getShow()
    {
        return $this->show;
    }

    /**
     * @param mixed $show
     */
    public function setShow(SchoolShow $show)
    {
        $this->show = $show;
    }

    /**
     * @return mixed
     */
    public function getDeclinedDate()
    {
        return $this->declinedDate;
    }

    /**
     * @param mixed $declinedDate
     */
    public function setDeclinedDate(\DateTime $declinedDate)
    {
        $this->declinedDate = $declinedDate;
    }


}