<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="school")
 */
class School
{
    const PARTICIPATION_LEVEL_FULL = 0;
    const PARTICIPATION_LEVEL_PARTIAL = 1;
    const PARTICIPATION_LEVEL_LIMITED = 2;

    const NEWSPAPER_PUBLISH_TWO = 5;
    const NEWSPAPER_PUBLISH_ONE = 4;
    const NEWSPAPER_PUBLISH_UNDECIDED = 3;
    const NEWSPAPER_PUBLISH_REFUSED = 2;
    const NEWSPAPER_PUBLISH_NO_PAPER = 1;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string")
     * @Assert\Length(
     *      min = 2,
     *      max = 2,
     *      exactMessage="Your school designation can only be 2 characters"
     * )
     */
    private $designation;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Address", cascade={"persist"})
     */
    private $address;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $phone1;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $phone1ext;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $phone2;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $phone2ext;

    function __construct()
    {
        //$this->address = new Address();
    }

    function __toString(){
        return (string)$this->name;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * @param mixed $designation
     */
    public function setDesignation($designation)
    {
        $this->designation = strtoupper($designation);
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getPhone1()
    {
        return $this->phone1;
    }

    /**
     * @param mixed $phone1
     */
    public function setPhone1($phone1)
    {
        $this->phone1 = $phone1;
    }

    /**
     * @return mixed
     */
    public function getPhone1ext()
    {
        return $this->phone1ext;
    }

    /**
     * @param mixed $phone1ext
     */
    public function setPhone1ext($phone1ext)
    {
        $this->phone1ext = $phone1ext;
    }

    /**
     * @return mixed
     */
    public function getPhone2()
    {
        return $this->phone2;
    }

    /**
     * @param mixed $phone2
     */
    public function setPhone2($phone2)
    {
        $this->phone2 = $phone2;
    }

    /**
     * @return mixed
     */
    public function getPhone2ext()
    {
        return $this->phone2ext;
    }

    /**
     * @param mixed $phone2ext
     */
    public function setPhone2ext($phone2ext)
    {
        $this->phone2ext = $phone2ext;
    }



}