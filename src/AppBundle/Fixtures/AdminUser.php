<?php
namespace AppBundle\Fixtures;

use AppBundle\Entity\Group;
use AppBundle\Entity\Season;
use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminUser extends Fixture implements DependentFixtureInterface
{

    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setUsername('curtis.holland');
        $user->setEmail('curtis.holland@gmail.com');
        $password = $this->encoder->encodePassword($user, 'xrnwt95');
        $user->setPassword($password);
        $user->addRole(User::ROLE_SUPER_ADMIN);
        $user->setEnabled(true);
        $user->setFName('Curtis');
        $user->setLName('Holland');
        $user->setMobilePhone('(216) 216-509-8523');
        $user->setHomePhone('(216) 321-4655');

        $manager->persist($user);
        $manager->flush();
    }


    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            Groups::class,
            CurrentSeason::class
        ];
    }
}