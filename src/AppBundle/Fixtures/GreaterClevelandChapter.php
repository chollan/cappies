<?php

namespace AppBundle\Fixtures;

use AppBundle\Entity\Address;
use AppBundle\Entity\BoardMember;
use AppBundle\Entity\Chapter;
use AppBundle\Entity\SeasonalSchool;
use AppBundle\Service\SeasonService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class GreaterClevelandChapter extends Fixture implements DependentFixtureInterface
{
    private $seasonService;
    private $manager;

    function __construct(SeasonService $seasonService)
    {
        $this->seasonService = $seasonService;
    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $chapter = $this->generateChapter();
        $chapter = $this->addBoardMembers($chapter);
        $this->addParticipatingSchools($chapter);
    }

    private function addParticipatingSchools(Chapter $chapter){
        $seasonalSchoolRepo = $this->manager->getRepository(SeasonalSchool::class);
        $seasonalSchools = $seasonalSchoolRepo->findAll();
        foreach($seasonalSchools as $school){
            $school->setChapter($chapter);
            $this->manager->persist($school);
        }
        $this->manager->persist($chapter);
        $this->manager->flush();
        return $chapter;
    }

    private function addBoardMembers(Chapter $chapter){
        $userRepo = $this->manager->getRepository(BoardMember::class);
        $boardMembers = $userRepo->findAll();
        foreach($boardMembers as $member){
            $chapter->addBoardMember($member);
        }
        $this->manager->persist($chapter);
        $this->manager->flush();
        return $chapter;
    }

    private function generateChapter(){
        $chapter = new Chapter();
        $chapter->setDesignation('CLE');
        $chapter->setName('Greater Cleveland Cappies');
        $chapter->setSeason($this->seasonService->getCurrentSeason());
        $chapter->setEin('12-1234567');
        $chapter->setBoundaries("Between Lake Erie and Lake Michigan.");
        $chapter->setDistance("about 45 minutes");
        $chapter->setTimezone(Chapter::TIMEZONE_EASTERN);
        $chapter->setSchoolInfo('Lorem ipsum');
        $chapter->setProgramType('RA');
        $chapter->setMinimumReviews(5);
        $chapter->setAwardtype("Gala");
        $chapter->setFees(400);
        $chapter->setInitSchoolAppDeadline(new \DateTime('2018-10-01'));
        $chapter->setInitCriticTrainingDate(new \DateTime('2018-10-01'));
        $chapter->setMidSchoolAppDeadline(new \DateTime('2018-10-01'));
        $chapter->setMidCriticTrainingDate(new \DateTime('2018-10-01'));
        //$chapter->setVoteDate();
        //$chapter->setAwardDate();
        $chapter->setReviewsRequiredToVote(5);
        $chapter->setMinMeanEvalScore('5.5');
        //$chapter->setAwardDate();
        //$chapter->setVotingStartTime();
        //$chapter->setVotingEndTime();
        $chapter->setVotingLocation("Lorem ipsum");
        $chapter->getGalaDateTime(new \DateTime('2018-12-05'));
        $chapter->setGalaLocation(
            $this->generateAddress()
        );
        $chapter->setStatus(Chapter::STATUS_APPROVED);
        $this->manager->persist($chapter);
        $this->manager->flush();
        return $chapter;
    }

    private function generateAddress(){
        $address = new Address();
        $address->setAddressLine1('1234 Gala Address Street');
        $address->setCity('Cleveland Heights');
        $address->setState('Ohio');
        $address->setZip('44118');
        return $address;
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            BoardMemberUsers::class,
            GreaterClevelandSchoolsAndUsers::class
        ];
    }
}