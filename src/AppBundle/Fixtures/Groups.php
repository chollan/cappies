<?php
namespace AppBundle\Fixtures;

use AppBundle\Entity\Group;
use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class Groups extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

        # Super Admin
        $manager->persist(
            new Group(Group::SUPER_ADMIN, [User::ROLE_SUPER_ADMIN], true)
        );

        # School Scaff
        $manager->persist(
            new Group(Group::ADVISER, [User::ROLE_SCHOOL_ADVISER], true)
        );
        $manager->persist(
            new Group(Group::SHOW_DIRECTOR, [User::ROLE_SHOW_DIRECTOR], true)
        );
        $manager->persist(
            new Group(Group::MENTOR, [User::ROLE_SCHOOL_MENTOR], true)
        );

        # Board Members
        $manager->persist(
            new Group(Group::CHAIRMAN, [User::ROLE_CHAIRMAN], true)
        );
        $manager->persist(
            new Group(Group::PROGRAM_DIRECTOR, [User::ROLE_PROGRAM_DIRECTOR], true)
        );
        $manager->persist(
            new Group(Group::TREASURER, [User::ROLE_TREASURER], true)
        );
        $manager->persist(
            new Group(Group::PROGRAM_MANAGER, [User::ROLE_PROGRAM_MANAGER], true)
        );
        $manager->persist(
            new Group(Group::STEERING_COMMITTEE_MEMBER, [User::ROLE_STEERING_MEMBER], true)
        );

        # Students
        $manager->persist(
            new Group(Group::CRITIC, [User::ROLE_CRITIC], true)
        );
        $manager->persist(
            new Group(Group::LEAD_CRITIC, [User::ROLE_LEAD_CRITIC], true)
        );

        # non-functional users
        $manager->persist(
            new Group(Group::BOOSTER, [], false)
        );
        $manager->persist(
            new Group(Group::ACCOUNTS_PAYABLE, [], false)
        );
        $manager->persist(
            new Group(Group::SCHOOL_OFFICIAL, [], false)
        );

        $manager->flush();
    }
}