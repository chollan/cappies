<?php
namespace AppBundle\Fixtures;

use AppBundle\Entity\Address;
use AppBundle\Entity\BoardMember;
use AppBundle\Entity\Group;
use AppBundle\Entity\Season;
use AppBundle\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class BoardMemberUsers extends Fixture implements DependentFixtureInterface
{

    private $encoder;
    private $groupRepo;
    private $manager;
    private $currentSeason;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $this->groupRepo = $manager->getRepository(Group::class);
        $this->manager = $manager;
        $this->currentSeason = $this->manager->getRepository(Season::class)->getCurrentSeason();
        $this->Chairman();
        $this->ProgramDirector();
        $this->ProgramManager();
        $this->Treasurer();
        $this->SteeringCommitteeMember();
    }

    private function Chairman(){
        $this->__create(Group::CHAIRMAN);
    }

    private function ProgramDirector(){
        $this->__create(Group::PROGRAM_DIRECTOR);
    }

    private function Treasurer(){
        $this->__create(Group::TREASURER);
    }

    private function ProgramManager(){
        $this->__create(Group::PROGRAM_MANAGER);
    }

    private function SteeringCommitteeMember(){
        $this->__create(Group::STEERING_COMMITTEE_MEMBER);
    }

    private function __create($role){
        $roleLowered = strtolower(str_replace(' ','',$role));
        $user = new User();
        $user->setEmail($roleLowered.'@gmail.com');
        $user->setPassword(
            $this->encoder->encodePassword($user, $roleLowered)
        );
        $user->setEnabled(true);
        $user->setFName($role);
        $user->setLName('User');
        $user->setMobilePhone('(216) 332-1121');
        $user->setHomePhone('(216) 990-4421');
        $this->manager->persist($user);

        $address = new Address();
        $address->setAddressLine1('1234 '.$role.' Way');
        $address->setCity('Cleveland Heights');
        $address->setState('Ohio');
        $address->setZip('44118');


        $seasonalUser = new BoardMember();
        $seasonalUser->setUser($user);
        $seasonalUser->setSeason($this->currentSeason);
        $seasonalUser->setAddress($address);
        $seasonalUser->setGroup(
            $this->groupRepo->findOneByName($role)
        );
        $seasonalUser->setHomePhone('(216) 691-7309');
        $seasonalUser->setMobilePhone('(216) 691-7309');
        $seasonalUser->setOfficePhone('(216) 691-7309');
        $this->manager->persist($seasonalUser);
        $this->manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    public function getDependencies()
    {
        return [
            Groups::class,
            CurrentSeason::class
        ];
    }
}