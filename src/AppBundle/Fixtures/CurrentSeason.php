<?php
namespace AppBundle\Fixtures;

use AppBundle\Entity\Season;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CurrentSeason extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        //August 1 - July 31
        $season = new Season();
        $season->setStartDate(new \DateTime('2016-08-01'));
        $season->setEndDate(new \DateTime('2017-07-31'));
        $season->setYear(2017);
        $manager->persist($season);

        $season = new Season();
        $season->setStartDate(new \DateTime('2017-08-01'));
        $season->setEndDate(new \DateTime('2018-07-31'));
        $season->setYear(2018);
        $manager->persist($season);

        $season = new Season();
        $season->setStartDate(new \DateTime('2018-08-01'));
        $season->setYear(2019);
        $manager->persist($season);

        $manager->flush();
    }
}