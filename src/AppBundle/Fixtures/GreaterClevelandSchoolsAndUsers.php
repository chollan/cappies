<?php
namespace AppBundle\Fixtures;

use AppBundle\Entity\Address;
use AppBundle\Entity\BoardMember;
use AppBundle\Entity\Critic;
use AppBundle\Entity\CriticTeam;
use AppBundle\Entity\Group;
use AppBundle\Entity\School;
use AppBundle\Entity\SchoolStaff;
use AppBundle\Entity\Season;
use AppBundle\Entity\SeasonalSchool;
use AppBundle\Entity\User;
use AppBundle\Service\SeasonService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class GreaterClevelandSchoolsAndUsers extends Fixture
{
    private $manager;
    private $seasonService;
    private $encoder;
    private $groupRepo;

    function __construct(SeasonService $seasonService, UserPasswordEncoderInterface $encoder)
    {
        $this->seasonService = $seasonService;
        $this->encoder = $encoder;

    }

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->groupRepo = $manager->getRepository(Group::class);
        $this->Roxboro();
        $this->Coventry();
        $this->CHHS();
    }

    private function Roxboro(){
        $address = new Address();
        $address->setAddressLine1('3624 Roxboro Rd');
        $address->setCity('Cleveland Heights');
        $address->setState('Ohio');
        $address->setZip('44118');

        $school = new School();
        $school->setName('Roxboro Middle School');
        $school->setDesignation("RM");
        $school->setAddress($address);
        $school->setPhone1('(216) 371-7440');
        $this->manager->persist($school);
        $this->manager->flush();

        $seasonalSchool = $this->setSeasonalSchool($school);

        $this->Users($seasonalSchool);

        $criticTeam = new CriticTeam();
        $criticTeam->setSchool($seasonalSchool);
        $criticTeam->setName('Roxboro Middle School Team');
        $criticTeam->setRegional(false);
        $this->manager->persist($criticTeam);
        $this->manager->flush();
    }

    private function Coventry(){
        $address = new Address();
        $address->setAddressLine1('2843 Washington Blvd');
        $address->setCity('Cleveland Heights');
        $address->setState('Ohio');
        $address->setZip('44118');

        $school = new School();
        $school->setName('Coventry Elementary School');
        $school->setDesignation("CE");
        $school->setAddress($address);
        $school->setPhone1('(216) 571-4676');
        $this->manager->persist($school);
        $this->manager->flush();

        $seasonalSchool = $this->setSeasonalSchool($school);

        $this->Users($seasonalSchool);

        $criticTeam = new CriticTeam();
        $criticTeam->setSchool($seasonalSchool);
        $criticTeam->setName('Coventry Elementary School Team');
        $criticTeam->setRegional(false);
        $this->manager->persist($criticTeam);
        $this->manager->flush();
    }

    private function Users(SeasonalSchool $seasonalSchool){
        //$roleLowered = strtolower(str_replace(' ','',$role));
        $schoolName = $seasonalSchool->getSchool()->getName();
        foreach([Group::LEAD_CRITIC, Group::CRITIC] as $userGroup){
            $email = strtolower(str_replace(' ','',$schoolName.$userGroup));
            $user = new User();
            $user->setEmail($email.'@gmail.com');
            $user->setPassword(
                $this->encoder->encodePassword($user, $email)
            );
            $user->setEnabled(true);
            $user->setFName($schoolName.' '.$userGroup);
            $user->setLName('User');
            $user->setMobilePhone('(216) 509-1121');
            $user->setHomePhone('(216) 371-7114');
            $this->manager->persist($user);


            $seasonalUser = new Critic();
            $seasonalUser->setUser($user);
            $seasonalUser->setSeason($this->seasonService->getCurrentSeason());
            $seasonalUser->setGrade(rand ( 0 , 3 ));
            $seasonalUser->setGroup(
                $this->groupRepo->findOneByName($userGroup)
            );
            $this->manager->persist($seasonalUser);
            $seasonalSchool->addParticipant($seasonalUser);
            $this->manager->flush();
        }

        foreach([Group::ADVISER, Group::SHOW_DIRECTOR, Group::MENTOR, Group::SCHOOL_OFFICIAL] as $userGroup){
            $email = strtolower(str_replace(' ','',$schoolName.$userGroup));
            $user = new User();
            $user->setEmail($email.'@gmail.com');
            $user->setPassword(
                $this->encoder->encodePassword($user, $email)
            );
            $user->setEnabled(true);
            $user->setFName($schoolName.' '.$userGroup);
            $user->setLName('User');
            $user->setMobilePhone('(216) 405-4125');
            $user->setHomePhone('(216) 321-4454');
            $this->manager->persist($user);

            $seasonalUser = new SchoolStaff();
            $seasonalUser->setUser($user);
            $seasonalUser->setSeason($this->seasonService->getCurrentSeason());
            $seasonalUser->setGroup(
                $this->groupRepo->findOneByName($userGroup)
            );
            $seasonalUser->setOfficePhone('(216) 691-7390');
            $this->manager->persist($seasonalUser);
            $seasonalSchool->addParticipant($seasonalUser);
            $this->manager->flush();
        }

    }

    private function CHHS(){
        $address = new Address();
        $address->setAddressLine1('13263 Cedar Rd');
        $address->setCity('Cleveland Heights');
        $address->setState('Ohio');
        $address->setZip('44118');

        $school = new School();
        $school->setName('Clevland Heights High School');
        $school->setDesignation("CH");
        $school->setAddress($address);
        $school->setPhone1('(216) 371-7101');
        $this->manager->persist($school);
        $this->manager->flush();

        $seasonalSchool = $this->setSeasonalSchool($school);

        $this->Users($seasonalSchool);

        $criticTeam = new CriticTeam();
        $criticTeam->setSchool($seasonalSchool);
        $criticTeam->setName('Cleveland Heights Team');
        $criticTeam->setRegional(false);
        $this->manager->persist($criticTeam);
        $this->manager->flush();
    }

    private function setSeasonalSchool(School $school){
        $seasonalSchool = new SeasonalSchool();
        $seasonalSchool->setSeason($this->seasonService->getCurrentSeason());
        $seasonalSchool->setSchool($school);
        $seasonalSchool->setStatus(SeasonalSchool::STATUS_APPROVED);
        $seasonalSchool->setNewspaperPublishStatus(School::NEWSPAPER_PUBLISH_REFUSED);
        $seasonalSchool->setParticipationLevel(School::PARTICIPATION_LEVEL_FULL);
        $this->manager->persist($seasonalSchool);
        $this->manager->flush();
        return $seasonalSchool;
    }
}