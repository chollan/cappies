<?php
namespace AppBundle\Fixtures;

use AppBundle\Entity\NewspaperContact;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class NewspaperContacts extends Fixture
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for($i=1; $i<9; $i++){
            $npc = new NewspaperContact();
            $npc->setName('Newspaper Contact '.$i);
            $npc->setPublisher('Cappies Publisher '.$i);
            $npc->setDesignationCode('NPC-'.$i);
            $npc->setEmail('123@newsaper'.$i.'.com');
            $npc->setReviewPoints(rand(1,2));
            $num = '('.$i.$i.$i.') '.$i.$i.$i.'-'.$i.$i.$i.$i;
            $npc->setPhone($num);
            $npc->setArchived(false);
            $manager->persist($npc);
        }
        $manager->flush();
    }
}