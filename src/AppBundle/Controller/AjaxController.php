<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Chapter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Class AjaxController
 * @package AppBundle\Controller
 * @Route("/ajax")
 */
class AjaxController extends Controller
{
    /**
     * @Route("/chapter/{status}/{id}", name="approve_chapter", requirements={"status"="^approve|deny$", "id"="\d+"})
     * @ParamConverter("id", class="AppBundle:Chapter")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function approveChapterAction(Chapter $chapter, $status, Request $request){
        if(!$request->isXmlHttpRequest()){
            throw new AccessDeniedHttpException('This is not an AJAX request');
        }
        $em = $this->getDoctrine()->getManager();
        if($status == 'approve'){
            $chapter->setStatus(Chapter::STATUS_APPROVED);
        }else{
            $chapter->setStatus(Chapter::STATUS_REJECTED);
        }
        $em->persist($chapter);
        $em->flush();
        return new JsonResponse(null, 202);
    }
}