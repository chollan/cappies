<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Group;
use AppBundle\Entity\SeasonalSchool;
use AppBundle\Form\ChapterApplicationType;
use AppBundle\Form\SchoolApplicationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ApplicationController extends Controller
{
    /**
     * @Route("/apply/chapter", name="chapter_application")
     */
    public function chapterApplicationAction(Request $request)
    {
        $form = $this->createForm(ChapterApplicationType::class);
        $groupRepo = $this->getDoctrine()->getRepository(Group::class);
        $steeringCommitteeMember = $groupRepo->findOneByName(Group::STEERING_COMMITTEE_MEMBER);
        $form->handleRequest($request);
        if($form->isSubmitted()){
            if($form->isValid()){
                $chapter = $form->getData();
                //dump($chapter);exit;

                $em = $this->getDoctrine()->getManager();
                $em->persist($chapter);
                $em->flush();
                return $this->redirectToRoute('homepage');
            }
        }
        return $this->render('AppBundle:default:chapter_apply.html.twig', [
            'form' => $form->createView(),
            'committeeGroupId' => $steeringCommitteeMember->getId()
        ]);
    }

    /**
     * @Route("/apply/school", name="school_application")
     * @Route("/apply/school/{applyToken}", name="school_application_partial")
     * @ParamConverter("school", options={"mapping":{"applyToken" = "applicationLink"}, "strip_null":true})
     */
    public function schoolApplicationAction(Request $request, SeasonalSchool $school = null){
        if($school instanceof SeasonalSchool && $school->getStatus() != SeasonalSchool::STATUS_APPROVED_TO_APPLY){
            $this->addFlash('error', 'You can not enter an application for this school at this time.');
            return $this->redirectToRoute("homepage");
        }
        $form = $this->createForm(SchoolApplicationType::class, $school);
        $groupRepo = $this->getDoctrine()->getRepository(Group::class);
        $adviserMember = $groupRepo->findOneByName(Group::ADVISER);
        $form->handleRequest($request);
        if($form->isSubmitted()){
            if($form->isValid()){
                $seasonalSchool = $form->getData();
                //dump($seasonalSchool);exit;

                $em = $this->getDoctrine()->getManager();
                $em->persist($seasonalSchool);
                $em->flush();
                return $this->redirectToRoute('homepage');
            }
        }
        return $this->render('AppBundle:default:school_apply.html.twig', [
            'form' => $form->createView(),
            'adviserGroupId' => $adviserMember->getId()
        ]);
    }
}