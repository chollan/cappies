<?php
/**
 * Created by PhpStorm.
 * User: cholland
 * Date: 9/9/18
 * Time: 4:21 PM
 */

namespace AppBundle\Controller;


use AppBundle\Entity\SeasonalSchool;
use AppBundle\Security\Voter\SchoolVoter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class SchoolController
 * @package AppBundle\Controller
 * @Route("/school")
 */
class SchoolController extends Controller
{
    /**
     * @Route("/{seasonalSchool}", name="school", requirements={"seasonalSchool"="^[A-Z]{2}$"})
     * @ParamConverter("seasonalSchool", class="AppBundle:SeasonalSchool")
     */
    public function schoolAction(SeasonalSchool $seasonalSchool){
        $this->denyAccessUnlessGranted(SchoolVoter::READ, $seasonalSchool);

        return $this->render('AppBundle:default:school.html.twig', [
            'school' => $seasonalSchool
        ]);
    }
}