<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Chapter;
use AppBundle\Entity\Critic;
use AppBundle\Entity\CriticTeam;
use AppBundle\Entity\CriticTeamShowAssignment;
use AppBundle\Entity\MappedSuperclass\SeasonalUser;
use AppBundle\Entity\SchoolShow;
use AppBundle\Entity\Season;
use AppBundle\Entity\SeasonalSchool;
use AppBundle\Event\ChapterEvent;
use AppBundle\Event\SchoolEvent;
use AppBundle\Form\CriticAssignmentCollectionType;
use AppBundle\Form\CriticAssignmentType;
use AppBundle\Security\Voter\ChapterVoter;
use AppBundle\Security\Voter\SeasonalSchoolVoter;
use AppBundle\Service\SeasonService;
use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminController
 * @package AppBundle\Controller
 * @Route("/admin")
 */
class AdminController extends Controller
{
    private $seasonService;
    private $eventDispatcher;

    function __construct(SeasonService $seasonService, EventDispatcherInterface $eventDispatcher){
        $this->seasonService = $seasonService;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @Route("/dashboard", name="admin_dashboard")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function dashboardAction(){
        $chapterRepo = $this->getDoctrine()->getRepository(Chapter::class);
        $schoolsRepo = $this->getDoctrine()->getRepository(SeasonalSchool::class);
        $showRepo = $this->getDoctrine()->getRepository(SchoolShow::class);
        $currentSeason = $this->seasonService->getCurrentSeason();
        return $this->render('AppBundle:admin:dashboard.html.twig', [
            'chapters' => $chapterRepo->findBySeason($currentSeason),
            'schools' => $schoolsRepo->findBySeason($currentSeason),
            'shows' => $showRepo->findBySeason($currentSeason)
        ]);
    }

    /**
     * @Route("/chapter/{id}/{action}", name="chapter_admin", requirements={"action"="^approve|deny$"})
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @ParamConverter("id", class="AppBundle:Chapter")
     */
    public function chapterAdminAction(Chapter $chapter, $action=null){
        if(!is_null($action) && $this->isGranted(ChapterVoter::CHANGE_STATUS, $chapter)){
            $em = $this->getDoctrine()->getManager();
            $status = Chapter::STATUS_REJECTED;
            if($action == 'approve'){
                $status = Chapter::STATUS_APPROVED;
            }
            if($chapter->getStatus() != $status){
                $chapter->setStatus($status);
                $em->persist($chapter);
                $em->flush();
                $this->eventDispatcher->dispatch(ChapterEvent::STATUS_CHANGED, new ChapterEvent($chapter));
            }
            return $this->redirectToRoute('chapter_admin', ['id' => $chapter->getId()]);

        }
        return $this->render('AppBundle:admin:chapter.html.twig', [
            'chapter' => $chapter
        ]);
    }

    /**
     * @Route("/chapter/{id}/assignments", name="chapter_assignments")
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @ParamConverter("id", class="AppBundle:Chapter")
     */
    public function chapterAssignmentsAction(Chapter $chapter, Request $request){
        $this->denyAccessUnlessGranted(ChapterVoter::CREATE_ASSIGNMENTS, $chapter);

        $form = $this->createForm(CriticAssignmentCollectionType::class, null, ['chapter' => $chapter]);
        $form->handleRequest($request);
        if($form->isSubmitted()){
            if($form->isValid()){
                $assignments = $form->getData();
                foreach ($assignments['assignments'] as $assignment){
                    if($assignment instanceof CriticTeamShowAssignment){
                        $em = $this->getDoctrine()->getManager();
                        $em->persist($assignment);
                        $em->flush();
                    }
                }
                $this->addFlash('success', 'Your assignments have been saved');
                return $this->redirectToRoute('chapter_assignments', ['id' => $chapter->getId()]);
            }
        }
        return $this->render('AppBundle:admin:critic_assignments.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/school/{id}/{action}", name="school_admin", requirements={"action"="^approve|deny$"})
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     * @ParamConverter("id", class="AppBundle:SeasonalSchool")
     */
    public function schoolAdminAction(SeasonalSchool $seasonalSchool, $action=null){
        if(!is_null($action) && $this->isGranted(SeasonalSchoolVoter::CHANGE_STATUS, $seasonalSchool)){
            $em = $this->getDoctrine()->getManager();
            $status = SeasonalSchool::STATUS_REJECTED;
            if($action == 'approve'){
                $status = SeasonalSchool::STATUS_APPROVED;
            }
            if($seasonalSchool->getStatus() != $status){
                $seasonalSchool->setStatus($status);
                $em->persist($seasonalSchool);
                $em->flush();
                $this->eventDispatcher->dispatch(SchoolEvent::STATUS_CHANGED, new SchoolEvent($seasonalSchool));
            }
            return $this->redirectToRoute('school_admin', ['id' => $seasonalSchool->getId()]);
        }
        return $this->render('AppBundle:admin:seasonal_school.html.twig', [
            'seasonalSchool' => $seasonalSchool
        ]);
    }
}