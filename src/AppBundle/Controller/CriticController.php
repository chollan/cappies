<?php

namespace AppBundle\Controller;

use AppBundle\Event\ReviewEvent;
use AppBundle\Security\Voter\ReviewVoter;
use AppBundle\Security\Voter\SchoolVoter;
use AppBundle\Security\Voter\ShowVoter;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\AppBundle;
use AppBundle\Entity\Chapter;
use AppBundle\Entity\CriticDeclinedShow;
use AppBundle\Entity\CriticTeamShowAssignment;
use AppBundle\Entity\CriticVolunteer;
use AppBundle\Entity\Review;
use AppBundle\Entity\SchoolShow;
use AppBundle\Entity\SeasonalSchool;
use AppBundle\Form\ReviewType;
use AppBundle\Service\SalesforceService;
use AppBundle\Service\SeasonService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CriticController extends Controller
{
    private $seasonService;
    private $salesforceService;

    function __construct(SeasonService $seasonService, SalesforceService $salesforceService)
    {
        $this->seasonService = $seasonService;
        $this->salesforceService = $salesforceService;
    }

    /**
     * @Route("/assignments", name="critic_assignments")
     * @Route("/assignments/decline/{show}", name="decline_assignment", requirements={"id"="\d+"})
     * @ParamConverter("show", class="AppBundle:SchoolShow")
     * @Security("is_granted('ROLE_CRITIC')")
     */
    public function criticAssignmentsAction(Request $request, SchoolShow $show = null)
    {
        $criticUser = $this->getUser()->getActiveSeason();
        if($show instanceof SchoolShow){
            $review = $show->getReviews($criticUser);
            if(is_null($review) || $this->isGranted(ReviewVoter::CREATE_REVISION, $review)){
                $em = $this->getDoctrine()->getManager();
                $declinedShow = new CriticDeclinedShow(
                    $criticUser,
                    $show
                );
                $em->persist($declinedShow);
                $em->flush();
                $this->addFlash('success', 'The show \''.$show->getName().'\' has been successfully declined.');
            }else{
                $this->addFlash(AppBundle::FLASH_ERROR, 'You are not eligible to decline this show at this time.');
            }

            return $this->redirectToRoute('critic_assignments');
        }
        $schoolShowRepo = $this->getDoctrine()->getRepository(SchoolShow::class);

        return $this->render('AppBundle:critic:assignments.html.twig', [
            'shows' => $schoolShowRepo->findByCriticAssignment($criticUser),
            'volunteer' => $schoolShowRepo->findByCriticVolunteer($criticUser),
            'declined' => $schoolShowRepo->findByCriticDeclination($criticUser)
        ]);
    }

    /**
     * @Route("/assignments/review/{show}", name="critic_show_review", requirements={"show"="\d+"})
     * @ParamConverter("show", class="AppBundle:SchoolShow")
     * @Security("is_granted('ROLE_CRITIC')")
     */
    public function criticAuthorReview(Request $request, SchoolShow $show, EventDispatcherInterface $dispatcher){
        $criticUser = $this->getUser()->getActiveSeason();
        $reviewRepo = $this->getDoctrine()->getRepository(Review::class);
        $review = $reviewRepo->findOneBy(['critic' => $criticUser, 'show' => $show]);
        $content = null;
        if(!($review instanceof Review)){
            $review = new Review($show, $criticUser);
        }

        $form = $this->createForm(ReviewType::class, $review);
        $form->handleRequest($request);
        if($form->isSubmitted()){
            if($form->isValid()){
                $buttonClicked = $form->getClickedButton()->getName();
                $em = $this->getDoctrine()->getManager();
                $review = $form->getData();
                if($buttonClicked == ReviewType::SUBMIT_BUTTON_SUBMIT_REVIEW){
                    $this->addFlash(AppBundle::FLASH_SUCCESS, 'Your revision has been submitted for Review');
                    $review->setStatus(Review::STATUS_SUBMITTED_TO_MENTOR);
                    $dispatcher->dispatch(ReviewEvent::STATUS_CHANGED, new ReviewEvent($review));
                }else{
                    $this->addFlash(AppBundle::FLASH_SUCCESS, 'Your revision has been saved');
                }
                $em->persist($review);
                $em->flush();
                return $this->redirectToRoute('critic_show_review', [
                    'show' => $show->getId()
                ]);
            }
        }

        return $this->render('AppBundle:critic:review.html.twig', [
            'show' => $show,
            'form' => $form->createView(),
            'review' => $review
            /*'volunteer' => $schoolShowRepo->findByCriticVolunteer($criticUser),
            'declined' => $schoolShowRepo->findByCriticDeclination($criticUser)*/
        ]);
    }

    /**
     * @Route("/volunteer", name="critic_volunteer")
     * @Route("/volunteer/{show}", name="volunteer_for_show", requirements={"id"="\d+"})
     * @ParamConverter("show", class="AppBundle:SchoolShow")
     * @Security("is_granted('ROLE_CRITIC')")
     */
    public function criticVolunteerAction(Request $request, SchoolShow $show = null)
    {
        // get the chapter
        $chapter = $this->seasonService->getChapterFromUser($this->getUser());

        // get the critic user
        $criticUser = $this->getUser()->getActiveSeason();

        // get their assignment
        $teamShowAssigmentRepo = $this->getDoctrine()->getRepository(CriticTeamShowAssignment::class);
        $assignment = $teamShowAssigmentRepo->findOneByTeam($criticUser->getCriticTeam());
        $assignedShows = [];
        if($assignment instanceof CriticTeamShowAssignment){
            $assignedShows = $assignment->getShows();
        }

        // is this a request to volunteer
        if($show instanceof SchoolShow){
            //$this->denyAccessUnlessGranted(ShowVoter::CAN_VOLUNTER, $show);
            $em = $this->getDoctrine()->getManager();
            $em->persist(
                new CriticVolunteer($criticUser, $show)
            );
            $em->flush();
            $this->addFlash('success', "You have successfully volunteered for ".$show->getName());
            return $this->redirectToRoute('critic_volunteer');
        }

        // if the page loaded with a chapter
        if($chapter instanceof Chapter){
            // get the shows
            $showsRepo = $this->getDoctrine()->getRepository(SchoolShow::class);
            $Shows = $showsRepo->findByChapter($chapter);

            // get the volunteered shows
            $volunteerRepo = $this->getDoctrine()->getRepository(CriticVolunteer::class);
            $volunteeredShows = $volunteerRepo->findByCritic($criticUser);

            // filter currently volunteered shows
            $Shows = $Shows->filter(function(SchoolShow $show) use ($volunteeredShows){
                foreach ($volunteeredShows as $volunteeredShow) {
                    if($show == $volunteeredShow->getShow()){
                        return false;
                    }
                }
                return true;
            });

            // filter the assigned shows
            $Shows = $Shows->filter(function(SchoolShow $show) use ($assignedShows){
                return ($assignedShows->contains($show));
            });

            // display the template
            return $this->render('AppBundle:critic:critic_volunteer.html.twig', [
                'shows' => $Shows,
                'volunteered' => $volunteeredShows
            ]);
        }
        $this->addFlash('error', 'There was an error fetching the chapter');
        return $this->redirectToRoute('user_dashboard');
    }

    /**
     * @Route("/unvolunteer/{show}", name="unvolunteer_for_show", requirements={"id"="\d+"})
     * @ParamConverter("show", class="AppBundle:SchoolShow")
     */
    public function criticUnvolunteerAction(SchoolShow $show){
        $volunteerRepo = $this->getDoctrine()->getRepository(CriticVolunteer::class);
        $volunteer = $volunteerRepo->findOneBy([
            'critic' => $this->getUser()->getActiveSeason(),
            'show' => $show
        ]);
        if($volunteer instanceof CriticVolunteer){
            $em = $this->getDoctrine()->getManager();
            $em->remove($volunteer);
            $em->flush();
            $this->addFlash(AppBundle::FLASH_SUCCESS, 'You are no longer volunteering for the show '.$show.'.');
        }else{
            $this->addFlash(AppBundle::FLASH_ERROR, 'There was an error finding your volunteer.  Please try again later.');
        }
        return $this->redirectToRoute('critic_volunteer');
    }


    /**
     * @Route("/submitReview", name="critic_review_dashboard")
     * @Security("is_granted('ROLE_CRITIC')")
     */
    public function criticReviewDashboardAction(){
        $criticUser = $this->getUser()->getActiveSeason();
        $schoolShowRepo = $this->getDoctrine()->getRepository(SchoolShow::class);
        return $this->render('AppBundle:critic:reviews_dashboard.html.twig', [
            'shows' => $schoolShowRepo->findAllByCritic($criticUser)
        ]);
    }

    /**
     * @Route("/submittedReviews", name="critic_submitted_review_dashboard")
     * @Security("is_granted('ROLE_CRITIC')")
     */
    public function criticSubmittedReviewDashboardAction(){
        $criticUser = $this->getUser()->getActiveSeason();
        $reviewRepo = $this->getDoctrine()->getRepository(Review::class);
        return $this->render('AppBundle:critic:submitted_reviews_dashboard.html.twig', [
            'reviews' => $reviewRepo->findBy([
                'critic' => $criticUser,
                'status' => [
                    Review::STATUS_APPROVED,
                    Review::STATUS_SUBMITTED_TO_ADMIN,
                    Review::STATUS_SUBMITTED_TO_MENTOR
                ]
            ])
        ]);
    }

    /**
     * @Route("/sendEmal", name="critic_send_email")
     * @Security("is_granted('ROLE_CRITIC') or is_granted('ROLE_LEAD_CRITIC')")
     */
    public function sendEmail(Request $request, SeasonService $seasonService){
        $form = $this->createFormBuilder()
            ->add('message', TextareaType::class, ['required' => true, 'attr' => ['rows' => '25']])
            ->add('send', SubmitType::class, ['attr' => ['class'=>'btn btn-primary']])
            ->getForm()
        ;
        $form->handleRequest($request);
        if($form->isSubmitted()){
            /*$messageData = $form->get('message')->getData();
            $seasonalUser = $this->getUser()->getActiveSeason();
            dump($seasonalUser->getCriticTeam()->getMembers()->toArray());*/
            $this->addFlash(AppBundle::FLASH_INFO, 'Your message was received, but this tool is not yet configured.');
            return $this->redirectToRoute('user_dashboard');
            //exit;
        }
        return $this->render('AppBundle:critic:send_email.html.twig', [
            'form' => $form->createView()
        ]);
    }
}