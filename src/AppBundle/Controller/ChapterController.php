<?php
namespace AppBundle\Controller;

use AppBundle\Entity\Chapter;
use AppBundle\Security\Voter\ChapterVoter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * Class ChapterController
 * @package AppBundle\Controller
 * @Route("/chapter")
 */
class ChapterController extends Controller
{
    /**
     * @param Chapter $chapter
     * @ParamConverter("chapterIdentifier", class="AppBundle:Chapter")
     * @Route("/{chapterIdentifier}", name="chapter")
     */
    public function chapterAction(Chapter $chapter){
        $this->denyAccessUnlessGranted(ChapterVoter::READ, $chapter);
        dump($chapter);exit;
    }

    /**
     * @param Chapter $chapter
     * @ParamConverter("chapterIdentifier", class="AppBundle:Chapter")
     * @Route("/{chapterIdentifier}/shows", name="chapter_shows")
     */
    public function chapterShows(Chapter $chapter){
        $this->denyAccessUnlessGranted(ChapterVoter::READ, $chapter);
        return $this->render('AppBundle:chapter:shows.html.twig', [
            'chapter' => $chapter
        ]);
    }
}