<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Chapter;
use AppBundle\Entity\CriticDeclinedShow;
use AppBundle\Entity\CriticTeamShowAssignment;
use AppBundle\Entity\CriticVolunteer;
use AppBundle\Entity\Review;
use AppBundle\Entity\SchoolShow;
use AppBundle\Entity\SeasonalSchool;
use AppBundle\Form\ReviewType;
use AppBundle\Service\SalesforceService;
use AppBundle\Service\SeasonService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    private $seasonService;
    private $salesforceService;

    function __construct(SeasonService $seasonService, SalesforceService $salesforceService)
    {
        $this->seasonService = $seasonService;
        $this->salesforceService = $salesforceService;
    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request){
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->redirectToRoute('user_dashboard');
        }
        //wykohakivu@gmail.com
        return $this->render('AppBundle:default:index.html.twig');
    }

    /**
     * @Route("/dashboard", name="user_dashboard")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function dashboardAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            return $this->redirectToRoute('admin_dashboard');
        }
        return $this->render('AppBundle:default:dashboard.html.twig');
    }

    /**
     * @Route("/chapterShows", name="chapter_shows")
     * @Security("is_granted('ROLE_CRITIC')")
     */
    public function chapterShows(){
        // get the chapter
        $chapter = $this->seasonService->getChapterFromUser($this->getUser());
        // get the shows
        $showsRepo = $this->getDoctrine()->getRepository(SchoolShow::class);
        $Shows = $showsRepo->findByChapter($chapter);
        // display the template
        return $this->render('AppBundle:default:chapter_shows.html.twig', [
            'shows' => $Shows
        ]);
    }

    /*
     * @Route("/sftest", name="sf_test")
     */
    public function salesforceTest(){
        /*$chapterRepo = $this->getDoctrine()->getRepository(Chapter::class);
        $this->salesforceService->sendChapterToSalesforce(
            $chapterRepo->find(1)
        );*/

        $SeasonalSchoollRepo = $this->getDoctrine()->getRepository(SeasonalSchool::class);
        $this->salesforceService->sendSchoolToSalesforce(
            $SeasonalSchoollRepo->find(1)
        );

        exit;
    }

}
