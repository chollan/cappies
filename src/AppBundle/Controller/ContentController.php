<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ContentController extends Controller
{
    /**
     * @Route("/binders", name="content_binders")
     */
    public function bindersAction(){
        return $this->render('AppBundle:content:binders.html.twig');
    }

    /**
     * @Route("/forms-checklists", name="content_forms_checklists")
     */
    public function formsChecklistsAction(){
        return $this->render('AppBundle:content:formsChecklists.html.twig');
    }

    /**
     * @Route("/cis-instructions", name="content_cis_instructions")
     */
    public function cisInstructionsAction(){
        return $this->render('AppBundle:content:cisInstructions.html.twig');
    }

    /**
     * @Route("/rules", name="content_rules")
     */
    public function rulesAction(){
        return $this->render('AppBundle:content:rules.html.twig');
    }

    /**
     * @Route("/powerpoint", name="content_powerpoint")
     */
    public function powerPointAction(){
        return $this->render('AppBundle:content:powerPoint.html.twig');
    }
}