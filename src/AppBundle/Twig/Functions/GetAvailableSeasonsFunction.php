<?php

namespace AppBundle\Twig\Functions;

use AppBundle\Entity\Season;
use Doctrine\ORM\EntityManagerInterface;
use Twig\TwigFunction;

class GetAvailableSeasonsFunction extends \Twig_Extension
{
    private $em;

    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('getAvailableLoginSeasons', [$this, 'getAvailableLoginSeasons'])
        ];
    }

    public function getAvailableLoginSeasons(){
        $seasonRepo = $this->em->getRepository(Season::class);
        return $seasonRepo->findBy([], ['year' => 'desc']);
    }

}