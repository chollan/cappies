<?php
namespace AppBundle\Twig\Extension;

use AppBundle\Entity\Season;
use AppBundle\Entity\User;
use AppBundle\Service\SeasonService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class DatabaseGlobalsExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    protected $seasonService;
    protected $tokenStorage;

    public function __construct(SeasonService $seasonService, TokenStorageInterface $tokenStorage)
    {
        $this->seasonService = $seasonService;
        $this->tokenStorage = $tokenStorage;
    }


    public function getGlobals()
    {
        $token = $this->tokenStorage->getToken();
        $chapter = null;
        if(!is_null($token)){
            $user = $token->getUser();
            if($user instanceof User){
                $chapter = $this->seasonService->getChapterFromUser($user);
            }
        }
        return array (
            "currentSeason" => $this->seasonService->getCurrentSeason(),
            "currentChapter" => $chapter
        );
    }
}