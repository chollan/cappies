<?php

namespace AppBundle\Twig\Filter;

use AppBundle\Entity\Critic;
use AppBundle\Entity\SchoolShow;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Twig\TwigFilter;

class GetCriticCountForShowFilter extends \Twig_Extension
{
    private $em;

    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('assignedCritics', [$this, 'getAssignedCritics'])
        ];
    }

    public function getAssignedCritics(SchoolShow $show){
        $criticRepo = $this->em->getRepository(Critic::class);
        return $criticRepo->findAllByShow($show);
    }
}