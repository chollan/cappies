<?php

namespace AppBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class AppBundle extends Bundle
{
    const FLASH_SUCCESS = 'success';
    const FLASH_INFO = 'info';
    const FLASH_ERROR = 'error';
    const FLASH_WARNING = 'warning';

    function __construct()
    {
        date_default_timezone_set('America/New_York');
    }
}
