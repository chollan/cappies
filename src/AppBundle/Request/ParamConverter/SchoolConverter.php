<?php
namespace AppBundle\Request\ParamConverter;

use AppBundle\Entity\MappedSuperclass\SeasonalUser;
use AppBundle\Entity\School;
use AppBundle\Entity\Season;
use AppBundle\Entity\SeasonalSchool;
use AppBundle\Service\SeasonService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class SchoolConverter implements ParamConverterInterface
{
    private $em;
    private $seasonService;

    public function __construct(EntityManagerInterface $em, SeasonService $seasonService)
    {
        $this->em = $em;
        $this->seasonService = $seasonService;
    }

    /**
     * Stores the object in the request.
     *
     * @param ParamConverter $configuration Contains the name, class and options of the object
     *
     * @return bool True if the object has been successfully set, else false
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $schoolDesignation = $request->attributes->get('schoolIdentifier', false);
        if(!$schoolDesignation){
            return false;
        }

        $schoolRepo = $this->em->getRepository(School::class);
        $seasonalSchoolRepo = $this->em->getRepository(SeasonalSchool::class);
        $school = $schoolRepo->findOneByDesignation($schoolDesignation);
        if($school instanceof School){

            $seasonalSchool = $seasonalSchoolRepo->findOneBy([
                'school' => $school,
                'season' => $this->seasonService->getCurrentSeason()
            ]);

            if($seasonalSchool instanceof SeasonalSchool){
                $request->attributes->set('seasonalSchool', $seasonalSchool);
                return true;
            }
        }

        return false;
    }

    /**
     * Checks if the object is supported.
     *
     * @return bool True if the object is supported, else false
     */
    public function supports(ParamConverter $configuration)
    {
        return ($configuration->getName() == 'schoolIdentifier');
    }
}