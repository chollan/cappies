<?php
namespace AppBundle\Request\ParamConverter;

use AppBundle\Entity\Chapter;
use AppBundle\Service\SeasonService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;

class ChapterConverter implements ParamConverterInterface
{
    private $em;
    private $seasonService;

    public function __construct(EntityManagerInterface $em, SeasonService $seasonService)
    {
        $this->em = $em;
        $this->seasonService = $seasonService;
    }

    /**
     * Stores the object in the request.
     *
     * @param ParamConverter $configuration Contains the name, class and options of the object
     *
     * @return bool True if the object has been successfully set, else false
     */
    public function apply(Request $request, ParamConverter $configuration)
    {
        $chapter = $request->attributes->get('chapterIdentifier', false);
        if(!$chapter){
            return false;
        }

        $chapterRepo = $this->em->getRepository(Chapter::class);
        $chapter = $chapterRepo->findOneBy([
            'designation' => strtoupper($chapter),
            'season' => $this->seasonService->getCurrentSeason()
        ]);

        if($chapter instanceof Chapter){
            $request->attributes->set('chapter', $chapter);
            return true;
        }

        return false;
    }

    /**
     * Checks if the object is supported.
     *
     * @return bool True if the object is supported, else false
     */
    public function supports(ParamConverter $configuration)
    {
        return ($configuration->getName() == 'chapterIdentifier');
    }
}