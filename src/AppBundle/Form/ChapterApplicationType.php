<?php

namespace AppBundle\Form;

use AppBundle\Entity\BoardMember;
use AppBundle\Entity\Chapter;
use AppBundle\Entity\Group;
use AppBundle\Entity\School;
use AppBundle\Entity\SeasonalSchool;
use AppBundle\Entity\User;
use AppBundle\Service\SeasonService;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Exception\LogicException;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChapterApplicationType extends AbstractType
{
    private $em;
    private $seasonService;

    function __construct(EntityManagerInterface $em, SeasonService $seasonService){
        $this->em = $em;
        $this->seasonService = $seasonService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            # step one: chapter information
            ->add('name', null, [
                'label' => 'Chapter Name'
            ])
            ->add('designation', null, [
                'label' => 'Chapter 3 letter designation'
            ])
            ->add('season', null, [
                'required' => true,
                'query_builder' => function(EntityRepository $er){
                    //dump($er);exit;
                    return $er->createQueryBuilder('s')
                        ->orderBy('s.year', 'DESC')
                        ->setMaxResults(1)
                    ;
                }
            ])
            ->add('ein', null, [
                'label' => 'Employers\'s ID # (EIN):',
                'attr' => [
                    'placeholder' => '__-_______'
                ]

            ])


            # step two: Steering Committee
            ->add('boardMembers', CollectionType::class, [
                'entry_type' => BoardMemberType::class,
                //'allow_add' => true,
                'label' => false,
                'entry_options' => [
                    'role_filter' => User::ROLE_STEERING_MEMBER,
                    'label' => false,
                    'disable_position' => true
                ]
            ])

            # step three: Geo information
            ->add('timezone', ChoiceType::class, [
                'choices' => [
                    'Eastern' => Chapter::TIMEZONE_EASTERN,
                    'Central' => Chapter::TIMEZONE_CENTRAL,
                    'Mountain' => Chapter::TIMEZONE_MOUNTAIN,
                    'Pacific' => Chapter::TIMEZONE_PACIFIC,
                ]
            ])
            ->add('boundaries', null, [
                'label' => 'Geographical Boundaries'
            ])
            ->add('distance', null, [
                'label' => 'Estimated driving time between most distant schools'
            ])

            # step four: Participating Schools
            ->add('participatingSchools', CollectionType::class, [
                'label' => false,
                'entry_type' => SeasonalSchoolType::class,
                //'allow_add' => true,
                'entry_options' => [
                    'label' => false
                ]
            ])

            # step 5: program options
            ->add('programType', ChoiceType::class, [
                'choices' => [
                    'Reviews and Awards' => 'RA',
                    'Reviews Only' => 'R'
                ],
                'required' => true
            ])
            ->add('numberOfShows', ChoiceType::class, [
                'label' => 'Number of Shows per school',
                'choices' => [
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                ],
                'required' => true
            ])
            ->add('minimumReviews',ChoiceType::class, [
                'label' => 'Number of reviews required per critic',
                'choices' => [
                    '3' => '3',
                    '4' => '4',
                    '5' => '5'
                ],
                'required' => true
            ])
            ->add('minMeanEvalScore',ChoiceType::class, [
                'label' => 'Minimum mean evaluation score',
                'choices' => [
                    '3.0' => '3.0',
                    '3.25' => '3.25',
                    '3.5' => '3.5',
                    '3.75' => '3.75',
                    '4.0' => '4.0',
                    '4.25' => '4.25',
                    '4.5' => '4.5',
                    '4.75' => '4.75',
                    '5.0' => '5.0',
                    '5.25' => '5.25',
                    '5.5' => '5.5',
                    '5.75' => '5.75',
                    '6.0' => '6.0',
                ]
            ])
            ->add('fees', ChoiceType::class, [
                'label' => 'Chapter Application Fee (To be paid with written application)',
                'choices' => [
                    '$300' => '300'
                ],
                'required' => true
            ])
            ->add('galaLocation', AddressType::class)

            # Step 6: Schedule
            ->add('initSchoolAppDeadline', null, [
                'label' => 'Initial school application deadline'
            ])
            ->add('initCriticTrainingDate', null, [
                'label' => 'Initial critics training date'
            ])
            ->add('midSchoolAppDeadline', null, [
                'label' => 'Mid-year school application deadlin'
            ])
            ->add('midCriticTrainingDate', null, [
                'label' => 'Mid-year critics training date'
            ])
            ->add('awardVotingDate', DateType::class, [
                'label' => 'Voting date'
            ])
            ->add('awardDate', null, [
                'label' => 'Awards event date'
            ])
        ;
        $groupRepo = $this->em->getRepository(Group::class);
        $seasonService = $this->seasonService;
        $builder->addEventListener(
            FormEvents::POST_SET_DATA,
            function(FormEvent $event) use ($groupRepo,$seasonService){
                $data = $event->getData();
                $form = $event->getForm();
                if(is_null($data)){
                    $boardMembers = $form->get('boardMembers');
                    if($boardMembers instanceof Form){
                        $collection = new ArrayCollection();
                        $neededUsers = [
                            Group::CHAIRMAN,
                            Group::PROGRAM_DIRECTOR,
                            Group::TREASURER,
                            Group::STEERING_COMMITTEE_MEMBER,
                            Group::STEERING_COMMITTEE_MEMBER
                        ];
                        foreach($neededUsers as $position){
                            $seasonalUser = new BoardMember();
                            $seasonalUser->setSeason($seasonService->getCurrentSeason());
                            $seasonalUser->setGroup(
                                $groupRepo->findOneByName($position)
                            );
                            $collection->add($seasonalUser);
                        }
                        $boardMembers->setData($collection);
                    }

                    $schools = $form->get('participatingSchools');
                    if($boardMembers instanceof Form){
                        $schools->setData(new ArrayCollection([
                            new SeasonalSchool(),
                            new SeasonalSchool(),
                            new SeasonalSchool(),
                            new SeasonalSchool()
                        ]));
                    }
                }
            }
        );

        $userRepo = $this->em->getRepository(User::class);
        $schoolRepo = $this->em->getRepository(School::class);
        $builder->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($userRepo, $groupRepo, $schoolRepo) {

                $chapter = $event->getData();

                if($chapter instanceof Chapter){
                    // uppercase the designation
                    $chapter->setDesignation(strtoupper($chapter->getDesignation()));

                    // get the season
                    $season = $chapter->getSeason();

                    // modify the board members
                    foreach($chapter->getBoardMembers() as $boardMember){

                        // see if the user exists
                        $existingUser = $userRepo->findOneByEmail($boardMember->getUser()->getEmail());

                        // we have an existing user
                        if(!is_null($existingUser)){
                            $boardMember->setUser($existingUser);
                        }

                        // set the season on the board member
                        $boardMember->setSeason($season);

                        // they are initially inactive
                        $boardMember->setActive(false);
                    }

                    // modify the participating schools
                    foreach($chapter->getParticipatingSchools() as $participatingSchool){
                        if($participatingSchool instanceof SeasonalSchool){
                            // get the school entered in the form
                            $requestedSchoolDesignation = $participatingSchool->getSchool()->getDesignation();

                            // get the school to see if it already exists
                            $school = $schoolRepo->findOneByDesignation($requestedSchoolDesignation);
                            if($school instanceof School){
                                $participatingSchool->setSchool($school);
                            }

                            // let's take a look at the member, there should only be 1
                            foreach($participatingSchool->getParticipants() as $seasonalParticipant){
                                $existingUser = $userRepo->findOneByEmail($seasonalParticipant->getUser()->getEmail());

                                if(!is_null($existingUser)){
                                    $seasonalParticipant->setUser($existingUser);
                                }

                                $group = $groupRepo->findOneByName('Adviser');
                                if(!($group instanceof Group)){
                                    throw new LogicException("Missing Adviser Role");
                                }

                                $seasonalParticipant->setSeason($season);
                                $seasonalParticipant->setGroup($group);
                                $seasonalParticipant->setActive(false);

                            }
                            $participatingSchool->setSeason($season);
                        }
                    }

                    // set the chapter status as pending
                    $chapter->setStatus(Chapter::STATUS_PENDING);
                }
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Chapter::class
        ]);
    }

}