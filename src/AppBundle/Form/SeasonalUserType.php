<?php
namespace AppBundle\Form;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SeasonalUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $rollFilter = $options['role_filter'];
        $bShowGroup = $options['show_group'];
        $disablePosition = $options['disable_position'];
        $builder
            ->add('user', UserType::class, [
                'label' => false
            ])
        ;
        if($bShowGroup){
            $builder->add('group', null, [
                'label' => 'Position',
                'required' => true,
                'disabled' => $disablePosition,
                'query_builder' => function(EntityRepository $er) use ($rollFilter){
                    $qb = $er->createQueryBuilder('g');
                    if(!is_null($rollFilter)){
                        $qb->where('g.roles LIKE :role')
                            ->setParameters([
                                'role' => '%'.$rollFilter.'%'
                            ]);
                    }
                    return $qb;
                }
            ]);
        }
        if($options['include_gender']){
            $builder->add('gender', ChoiceType::class, [
                'choices' => [
                    'Male' => 'M',
                    'Female' => 'F'
                ],
                'required' => true
            ]);
        }
        if($options['include_grade']){
            $builder->add('grade', ChoiceType::class, [
                'choices' => [
                    'Freshman' => User::GRADE_FRESHMAN,
                    'Sophomore' => User::GRADE_SOPHOMORE,
                    'Junior' => User::GRADE_JUNIOR,
                    'Senior' => User::GRADE_SENIOR
                ],
                'required' => true
            ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {

        $resolver->setDefaults([
            'role_filter' => null,
            'show_group' => true,
            'include_grade' => false,
            'include_gender' => false,
            'disable_position' => false
        ]);
    }

}