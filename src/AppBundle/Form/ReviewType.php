<?php
namespace AppBundle\Form;

use AppBundle\Entity\Review;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReviewType extends AbstractType
{
    const SUBMIT_BUTTON_SAVE_DRAFT = 'saveDraft';
    const SUBMIT_BUTTON_SUBMIT_REVIEW = 'submitReview';


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('revision', ReviewRevisionType::class, [
                'label' => false
            ])
            ->add(self::SUBMIT_BUTTON_SUBMIT_REVIEW, SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-primary'
                ]
            ])
            ->add(self::SUBMIT_BUTTON_SAVE_DRAFT, SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-outline-secondary'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Review::class
        ]);
    }

}