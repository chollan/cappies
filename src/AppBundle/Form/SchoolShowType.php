<?php
namespace AppBundle\Form;

use AppBundle\Entity\SchoolShow;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SchoolShowType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'Musical' => SchoolShow::TYPE_MUSICAL,
                    'Play' => SchoolShow::TYPE_PLAY
                ]
            ])
            ->add('address', AddressType::class, [
                'required' => false
            ])
            ->add('addressSameAsSchool', CheckboxType::class, [
                'data' => true,
                'required' => false
            ])
            ->add('dates', CollectionType::class, [
                'entry_type' => DateTimeType::class,
                'allow_add' => true,
                'label' => 'Performance Dates',
                'prototype_name' => '__date_name__',
                'entry_options' => [
                    'label' => false,
                    //'widget' => 'single_text'
                    'widget' => 'choice'
                ]
            ])
            ->add('dressRehearsalDates', CollectionType::class, [
                'entry_type' => DateTimeType::class,
                'allow_add' => true,
                'label' => 'Dress Rehearsal Dates',
                'prototype_name' => '__date_name__',
                'entry_options' => [
                    'label' => false,
                    //'widget' => 'single_text'
                    'widget' => 'choice'
                ]
            ])
            ->add('reviewDressRehearsal', CheckboxType::class, [
                'required' => false
            ])
        ;
        /*$builder->get('dates')->addModelTransformer(
            new CallbackTransformer(
                function ($a){dump('fcn 1');dump($a);},
                function ($a){dump('fcn 2');dump($a);exit;}
            )
        );*/
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SchoolShow::class
        ]);
    }

}