<?php
namespace AppBundle\Form;

use AppBundle\Entity\Chapter;
use AppBundle\Entity\CriticTeam;
use AppBundle\Entity\CriticTeamShowAssignment;
use AppBundle\Entity\SeasonalSchool;
use AppBundle\Service\SeasonService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CriticAssignmentCollectionType extends AbstractType
{
    private $em;
    private $seasonService;

    function __construct(EntityManagerInterface $em, SeasonService $seasonService)
    {
        $this->em = $em;
        $this->seasonService = $seasonService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $chapter = $options['chapter'];
        $criticTeamRepo = $this->em->getRepository(CriticTeam::class);
        $criticTeamShowRepo = $this->em->getRepository(CriticTeamShowAssignment::class);
        $schoolRepo = $this->em->getRepository(SeasonalSchool::class);
        $schools = $schoolRepo->findByChapter($chapter);

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function(FormEvent $event) use ($criticTeamRepo, $criticTeamShowRepo, $chapter, $schools){
                $data = $event->getData();
                $form = $event->getForm();
                $formData = null;
                if(is_null($data) && ($chapter instanceof Chapter)){
                    $chapterCriticTeams = $criticTeamRepo->findByChapter($chapter);
                    $existingAssignments = $criticTeamShowRepo->findByChapter($chapter);

                    // build a matrix form
                    $formData = [];
                    foreach ($chapterCriticTeams as $criticTeam){
                        $assignment = new CriticTeamShowAssignment($criticTeam);
                        foreach($existingAssignments as $existingAssignment){
                            if($existingAssignment->getTeam() == $criticTeam){
                                $assignment = $existingAssignment;
                                break;
                            }
                        }
                        $formData[] = $assignment;
                    }
                }

                $form->add('assignments', CollectionType::class, [
                    'entry_type' => CriticAssignmentType::class,
                    'data' => $formData,
                    'entry_options' => [
                        'label' => false
                    ],
                    'label' => false
                ]);
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'chapter' => false
        ]);
    }

}