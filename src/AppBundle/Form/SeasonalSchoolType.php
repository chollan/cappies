<?php
namespace AppBundle\Form;

use AppBundle\Entity\Group;
use AppBundle\Entity\SchoolStaff;
use AppBundle\Entity\SeasonalSchool;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SeasonalSchoolType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('school', SchoolType::class)
            ->add('participants', CollectionType::class, [
                'label'=> Group::PROGRAM_DIRECTOR,
                'entry_type' => SchoolStaffType::class,
                'data' => [new SchoolStaff()],
                'entry_options' => [
                    'show_group' => false
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SeasonalSchool::class
        ]);
    }

}