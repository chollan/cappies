<?php
namespace AppBundle\Form;

use AppBundle\Entity\BoardMember;
use AppBundle\Entity\Chapter;
use AppBundle\Entity\Critic;
use AppBundle\Entity\School;
use AppBundle\Entity\SchoolStaff;
use AppBundle\Entity\Season;
use AppBundle\Entity\SeasonalSchool;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SchoolApplicationType extends AbstractType
{
    private $em;

    function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            # Step One: Choose Program
            ->add('chapter', null, [
                'required' => true,
                'placeholder' => 'Select Chapter',
                'query_builder' => function (EntityRepository $er){
                    return $er->createQueryBuilder('c')
                        ->where('c.status = :status')
                        ->setParameters(['status' => Chapter::STATUS_APPROVED]);
                }
            ])

            # Step Two: School Information
            ->add('school', SchoolType::class, [
                'include_address' => true,
                'label' => 'School Information'
            ])
            ->add('participationLevel', ChoiceType::class, [
                'expanded' => true,
                'choices' => [
                    'Full (awards, reviews, critics, mentors)' => School::PARTICIPATION_LEVEL_FULL,
                    'Partial (reviews, critics, mentors only)' => School::PARTICIPATION_LEVEL_PARTIAL,
                    'Limited (critics only)' => School::PARTICIPATION_LEVEL_LIMITED,
                ],
                'data' => School::PARTICIPATION_LEVEL_FULL,
                'required' => true
            ])

            # Step Three: Show Information
            ->add('shows', CollectionType::class, [
                'entry_type' => SchoolShowType::class,
                'allow_add' => true,
                //'label' => false,
                'entry_options' => [
                    'label' => false
                ]
            ])

            # Step Four: Contact Information
            ->add('contacts', CollectionType::class, [
                'entry_type' => SchoolStaffType::class,
                'entry_options' => [
                    'role_filter' => User::ROLE_SCHOOL_STAFF,
                    'label' => false
                ],
                'allow_add' => true
            ])

            # Step Five: Critic Team
            ->add('critics', CollectionType::class, [
                'entry_type' => CriticType::class,
                'allow_add' => true,
                'entry_options' => [
                    'label' => false,
                    'role_filter' => User::ROLE_SCHOOL_STUDENT,
                    'include_gender' => true,
                    'include_grade' => true
                ]
            ])

            # Step Six: School Newspaper Information
            ->add('newspaper', null, [
                'label' => 'Name of School Newspaper'
            ])
            ->add('newspaperPublishStatus', ChoiceType::class, [
                'expanded' => true,
                'label' => 'Our School Newspaper',
                'choices' => [
                    'Has agreed to publish two Cappies reviews of each Cappies Show at our school.' => School::NEWSPAPER_PUBLISH_TWO,
                    'Has agreed to publish one Cappies review of each Cappies Show at our school.' => School::NEWSPAPER_PUBLISH_ONE,
                    'Has not decided whether to publish any Cappies reviews.' => School::NEWSPAPER_PUBLISH_UNDECIDED,
                    'Has refused to publish any Cappies reviews.' => School::NEWSPAPER_PUBLISH_REFUSED,
                    'Our school has no School Newspaper.' => School::NEWSPAPER_PUBLISH_NO_PAPER,
                ],
                'required' => true,
                'data' => School::NEWSPAPER_PUBLISH_UNDECIDED
            ])

            # Step Seven: Unavailable Dates
            ->add('criticsUnavailableDates', CollectionType::class, [
                'entry_type' => DateType::class,
                'allow_add' => true,
                'label' => 'Critic Unavailable Dates',
                'entry_options' => [
                    'widget' => 'single_text',
                    //'widget' => 'choice',
                    'label' => false
                ]
            ])
        ;

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                $school = $event->getData();
                if($school instanceof SeasonalSchool){
                    $participants = $school->getParticipants();
                    foreach($participants as $participant){
                        if($participant instanceof SchoolStaff){
                            $school->addContact($participant);
                        }
                        if($participant instanceof Critic){
                            $school->addCritic($participant);
                        }
                    }
                }
                //
                return $school;
            }
        );

        $schoolRepo = $this->em->getRepository(School::class);
        $userRepo = $this->em->getRepository(User::class);
        $builder->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($schoolRepo, $userRepo) {

                $school = $event->getData();
                if($school instanceof SeasonalSchool){
                    $season = $school->getChapter()->getSeason();
                    $school->setSeason($season);

                    $schoolRecord = $school->getSchool();
                    if($schoolRecord instanceof School){
                        // uppercase school designation
                        $designation = $schoolRecord->getDesignation();
                        $schoolRecord->setDesignation(strtoupper($designation));

                        // let's look for existing school
                        $existingSchool = $schoolRepo->findOneByDesignation(
                            $schoolRecord->getDesignation()
                        );
                        if(!is_null($existingSchool)){
                            $school->setSchool($existingSchool);
                        }
                    }

                    // find existing school contact users and update
                    $aryContacts = $school->getContacts();
                    foreach($aryContacts as $contact){
                        $contact->setSeason($season);
                        $contact->setActive(false);
                        $user = $contact->getUser();
                        $existingUser = $userRepo->findOneByEmail($user->getEmail());
                        if(!is_null($existingUser)){
                            $contact->setUser($existingUser);
                        }
                        $school->addParticipant($contact);
                    }

                    // find existing critic users and update
                    $aryCritics = $school->getCritics();
                    foreach($aryCritics as $critic){
                        $critic->setSeason($season);
                        $critic->setActive(false);
                        $user = $critic->getUser();
                        $existingUser = $userRepo->findOneByEmail($user->getEmail());
                        if(!is_null($existingUser)){
                            $critic->setUser($existingUser);
                        }
                        $school->addParticipant($critic);
                    }

                    $aryShows = $school->getShows();
                    foreach($aryShows as $show){
                        $show->setSchool($school);
                    }
                    $school->setStatus(SeasonalSchool::STATUS_PENDING);
                }
            }
        );

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SeasonalSchool::class
        ]);
    }

}