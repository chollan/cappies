<?php
namespace AppBundle\Form;

use AppBundle\Entity\Chapter;
use AppBundle\Entity\CriticTeam;
use AppBundle\Entity\CriticTeamShowAssignment;
use AppBundle\Entity\SchoolShow;
use AppBundle\EntityRepository\SchoolShowRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CriticAssignmentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('team', EntityType::class, [
                'disabled' => true,
                'class' => CriticTeam::class,
                'label' => false
            ])
        ;

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function(FormEvent $event){
                $data = $event->getData();
                $form = $event->getForm();
                $form
                    ->add('shows', EntityType::class, [
                        'class' => SchoolShow::class,
                        'expanded' => true,
                        'multiple' => true,
                        'label' => false,
                        'choice_label' => function (SchoolShow $show) {
                            //"show name" at "school name" at {time}
                            $showName = ''.$show->getName();
                            $schoolName = $show->getSchool()->getSchool()->getName();
                            $date = $show->getSelectedDate();
                            return $showName.' at '.$schoolName.' at '.$date->format('F jS @ g:i a');
                        },
                        'query_builder' => function(SchoolShowRepository $schoolShowRepository) use ($data){
                            if($data instanceof CriticTeamShowAssignment){
                                $team = $data->getTeam();
                                $qb = $schoolShowRepository
                                    ->createQueryBuilder('school_show')
                                    ->select()
                                    ->join('school_show.school', 'school')
                                    ->join ('school.criticTeam', 'criticTeam')
                                    ->where('criticTeam != :team')
                                    ->setParameters([
                                        'team' => $team
                                    ])
                                ;
                            }else{
                                $qb = $schoolShowRepository->createQueryBuilder('school_show');
                            }
                            $qb
                                ->addOrderBy('school_show.selectedDate')
                            ;
                            //dump($qb->getQuery()->getSQL());exit;
                            return $qb;
                        }
                    ])
                ;

            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CriticTeamShowAssignment::class
        ]);
    }

}