<?php
namespace AppBundle\Form;

use AppBundle\Entity\SchoolStaff;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SchoolStaffType extends SeasonalUserType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('officePhone', TelType::class, [
                'label' => false
            ])
            ->add('isAlsoShowDirector', CheckboxType::class, [
                'required' => false,
                'label' => 'This person is also acting as the Show Director'
            ])
            ->add('isAlsoMentor', CheckboxType::class, [
                'required' => false,
                'label' => 'This person is also acting as the Mentor'
            ])
        ;
        return parent::buildForm($builder, $options);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SchoolStaff::class
        ]);
        parent::configureOptions($resolver);
    }

}