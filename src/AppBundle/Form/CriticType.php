<?php
namespace AppBundle\Form;

use AppBundle\Entity\Critic;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CriticType extends SeasonalUserType
{

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Critic::class
        ]);
        parent::configureOptions($resolver);
    }

}