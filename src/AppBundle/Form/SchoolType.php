<?php
namespace AppBundle\Form;

use AppBundle\Entity\School;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SchoolType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $bIncludeAddress = $options['include_address'];
        $builder
            ->add('name')
            ->add('designation')
        ;
        if($bIncludeAddress){
            $builder
                ->add('address', AddressType::class)
            ;
        }

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => School::class,
            'include_address' => false
        ]);
    }

}