<?php
namespace AppBundle\Form;

use AppBundle\Entity\BoardMember;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BoardMemberType extends SeasonalUserType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('officePhone', TelType::class, [
                'label' => false
            ])
            ->add('address', AddressType::class)
        ;
        return parent::buildForm($builder, $options);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BoardMember::class,
        ]);
        parent::configureOptions($resolver);
    }

}