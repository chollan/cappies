<?php
namespace AppBundle\EntityRepository;

use AppBundle\Entity\Chapter;
use AppBundle\Entity\Critic;
use AppBundle\Entity\CriticDeclinedShow;
use AppBundle\Entity\CriticTeamShowAssignment;
use AppBundle\Entity\Season;
use AppBundle\Entity\SeasonalSchool;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;

class SchoolShowRepository extends EntityRepository
{
    public function findByChapter(Chapter $chapter, $bReturnQueryBuilder = false){
        $qb = $this->createQueryBuilder('ss');
        $query = $qb->select()
            ->join('ss.school', 'seasonalSchool')
            ->join('seasonalSchool.chapter', 'chapter')
            ->where('chapter = :chapter')
            ->andWhere('seasonalSchool.status = :status')
            ->setParameters([
                'chapter' => $chapter,
                'status' => SeasonalSchool::STATUS_APPROVED
            ])
        ;
        if($bReturnQueryBuilder)
            return $query;
        return new ArrayCollection(
            $query->getQuery()->getResult()
        );
    }

    public function findByCriticAssignment(Critic $critic){
        $declinedQg = $this->_em->createQueryBuilder()
            ->from(CriticDeclinedShow::class, 'declined')
        ;
        $qb = $this->_em->createQueryBuilder();
        $qb
            ->select('assigned_show, shows')
            ->from(CriticTeamShowAssignment::class, 'assigned_show')
            ->join('assigned_show.team', 'team')
            ->join('assigned_show.shows', 'shows')
            ->where(':critic MEMBER OF team.members')
            ->andWhere(
                $qb->expr()->notIn(
                    'shows',
                    $declinedQg
                        ->select('IDENTITY(declined.show)')
                        ->where(':critic = declined.critic')
                        ->getDQL()
                )
            )
            ->setParameter('critic', $critic)
        ;

        $result = $qb->getQuery()->getResult();
        $returnArray = new ArrayCollection();
        if(count($result) > 0){
            foreach ($result as $CriticTeamShowAssignment){
                $Shows = $CriticTeamShowAssignment->getShows();
                if(count($Shows) > 0){
                    foreach($Shows as $Show){
                        if(!$returnArray->contains($Show)){
                            $returnArray->add($Show);
                        }
                    }
                }
            }
        }

        return $returnArray;
    }

    public function findByCriticDeclination(Critic $critic){
        $qb = $this->_em->createQueryBuilder()
            ->select('declined, show')
            ->from(CriticDeclinedShow::class, 'declined')
            ->join('declined.show', 'show')
            ->where('declined.critic = :critic')
            ->setParameter('critic', $critic)
        ;
        $aryReturn = new ArrayCollection();
        foreach($qb->getQuery()->getResult() as $DeclinedShow){
            $show = $DeclinedShow->getShow();
            if(!$aryReturn->contains($show)){
                $aryReturn->add($show);
            }
        }
        return $aryReturn;
    }

    public function findByCriticVolunteer(Critic $critic){
        $qb = $this->createQueryBuilder('school_show');
        $qb
            ->select()
            ->where(':critic MEMBER OF school_show.volunteerCritics')
            ->setParameters([
                'critic' => $critic
            ])
        ;
        return new ArrayCollection(
            $qb->getQuery()->getResult()
        );
    }

    public function findAllByCritic(Critic $critic){
        return new ArrayCollection(
            array_merge(
                $this->findByCriticAssignment($critic)->toArray(),
                $this->findByCriticVolunteer($critic)->toArray()
            )
        );
    }

    public function findBySeason(Season $season){
        $qb = $this->createQueryBuilder('school_show');
        $qb
            ->select()
            ->join('school_show.school', 'school')
            ->join('school.season', 'season')
            ->where('season = :season')
            ->setParameter('season', $season)
        ;
        return new ArrayCollection(
            $qb->getQuery()->getResult()
        );
    }
}