<?php
namespace AppBundle\EntityRepository;

use AppBundle\Entity\SchoolShow;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityRepository;

class CriticRepository extends EntityRepository
{
    public function findByShowAssignment(SchoolShow $show){
        $qb = $this->createQueryBuilder('c');
        $qb2 = $this->createQueryBuilder('c2');
        $qb->select()
            ->join('c.criticTeam', 'team')
            ->join('team.assignments', 'assignments')
            ->where(':show MEMBER OF assignments.shows')
            ->andWhere(
                $qb->expr()->notIn(
                    'c',
                    $qb2->select('c2')
                    ->where(':show2 MEMBER OF c2.declinedShows')
                    ->getDQL()
                )
            )
            ->setParameters([
                'show' => $show,
                'show2' => $show
            ])
        ;
        return new ArrayCollection($qb->getQuery()->getResult());
    }

    public function findByShowVolunteer(SchoolShow $show){
        $qb = $this->createQueryBuilder('c')
            ->select()
            ->join('c.volunteerShows', 'volunteer_shows')
            ->where('volunteer_shows = :show')
            ->setParameters([
                'show' => $show
            ])
        ;
        return new ArrayCollection($qb->getQuery()->getResult());
    }

    public function findAllByShow(SchoolShow $show){
        return new ArrayCollection(
            array_merge(
                $this->findByShowAssignment($show)->toArray(),
                $this->findByShowVolunteer($show)->toArray()
            )
        );
    }
}