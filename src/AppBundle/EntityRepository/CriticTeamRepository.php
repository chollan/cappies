<?php
namespace AppBundle\EntityRepository;

use AppBundle\Entity\Chapter;
use Doctrine\ORM\EntityRepository;

class CriticTeamRepository extends EntityRepository
{
    public function findByChapter(Chapter $chapter){
        $qb = $this->createQueryBuilder('ct');
        $query = $qb->select()
            ->join('ct.school', 'school')
            ->join ('school.chapter', 'chapter')
            ->where('chapter = :chapter')
            ->setParameters([
                'chapter' => $chapter
            ])
        ;
        return $query->getQuery()->getResult();
    }
}