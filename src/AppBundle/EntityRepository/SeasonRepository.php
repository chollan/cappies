<?php
namespace AppBundle\EntityRepository;

use Doctrine\ORM\EntityRepository;

class SeasonRepository extends EntityRepository
{

    public function getCurrentSeason(){
        $qb = $this->createQueryBuilder('s');
        $query = $qb->select()
            ->where(
                $qb->expr()->between('CURRENT_TIMESTAMP()','s.startDate', 's.endDate')
            )
            ->orWhere(
                $qb->expr()->andX(
                    $qb->expr()->lte('s.startDate', ':startDate2'),
                    $qb->expr()->isNull('s.endDate')
                )
            )
            ->setMaxResults(1)
            ->orderBy('s.startDate')
            ->setParameters([
                'startDate2' => new \DateTime()
            ])
        ;
        return $query->getQuery()->getOneOrNullResult();
    }
}