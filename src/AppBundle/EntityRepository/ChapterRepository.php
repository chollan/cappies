<?php
namespace AppBundle\EntityRepository;

use AppBundle\Entity\BoardMember;
use AppBundle\Entity\Chapter;
use AppBundle\Entity\Critic;
use AppBundle\Entity\SchoolStaff;
use Doctrine\ORM\EntityRepository;

class ChapterRepository extends EntityRepository
{
    public function findOneByCritic(Critic $critic){
        $qb = $this->createQueryBuilder('c');
        $qb->select()
            ->join('c.participatingSchools', 'seasonalSchool')
            ->where(':critic MEMBER OF seasonalSchool.participants')
            ->andWhere('c.season = :season')
            ->setParameters([
                'critic' => $critic,
                'season' => $critic->getSeason()
            ])
            ->setMaxResults(1)
        ;
        $query = $qb->getQuery();
        return $query->getOneOrNullResult();
    }

    public function findOneByBoardMember(BoardMember $boardMember){
        $qb = $this->createQueryBuilder('c');
        $qb->select()
            ->where(':boardMember MEMBER OF c.boardMembers')
            ->andWhere('c.season = :season')
            ->setParameters([
                'boardMember' => $boardMember,
                'season' => $boardMember->getSeason()
            ])
            ->setMaxResults(1)
        ;
        $query = $qb->getQuery();
        return $query->getOneOrNullResult();
    }

    public function findOneBySchoolStaff(SchoolStaff $schoolStaff){
        $qb = $this->createQueryBuilder('c');
        $qb->select()
            ->join('c.participatingSchools', 'seasonalSchool')
            ->where(':schoolStaff MEMBER OF seasonalSchool.participants')
            ->andWhere('c.season = :season')
            ->setParameters([
                'schoolStaff' => $schoolStaff,
                'season' => $schoolStaff->getSeason()
            ])
            ->setMaxResults(1)
        ;
        $query = $qb->getQuery();
        return $query->getOneOrNullResult();
    }

    public function findShowsByChapter(Chapter $chapter){
        $qb = $this->createQueryBuilder('c');
        $qb->select()
            ->join('c.participatingSchools', 'schools')
            ->join('schools.shows', 'shows')
        ;
        $query = $qb->getQuery();
        dump($query->getResult());exit;
    }
}