<?php
namespace AppBundle\EntityRepository;

use AppBundle\Entity\Chapter;
use AppBundle\Entity\CriticTeam;
use Doctrine\ORM\EntityRepository;

class CriticTeamShowAssignmentRepository extends EntityRepository
{
    public function findByChapter(Chapter $chapter){
        $qb = $this->createQueryBuilder('ctsa');
        $qb->select()
            ->join('ctsa.team', 'critic_team')
            ->join('critic_team.school', 'school')
            ->join('school.chapter', 'chapter')
            ->where('chapter = :chapter')
            ->setParameters([
                'chapter' => $chapter
            ])
        ;
        return $qb->getQuery()->getResult();
    }
}